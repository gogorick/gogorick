
Debian
====================
This directory contains files used to package schmecklesd/schmeckles-qt
for Debian-based Linux systems. If you compile schmecklesd/schmeckles-qt yourself, there are some useful files here.

## schmeckles: URI support ##


schmeckles-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install schmeckles-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your schmeckles-qt binary to `/usr/bin`
and the `../../share/pixmaps/schmeckles128.png` to `/usr/share/pixmaps`

schmeckles-qt.protocol (KDE)

