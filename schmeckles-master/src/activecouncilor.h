// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ACTIVECOUNCILOR_H
#define ACTIVECOUNCILOR_H

#include "net.h"
#include "key.h"
#include "wallet/wallet.h"

class CActiveCouncilor;

static const int ACTIVE_COUNCILOR_INITIAL          = 0; // initial state
static const int ACTIVE_COUNCILOR_SYNC_IN_PROCESS  = 1;
static const int ACTIVE_COUNCILOR_INPUT_TOO_NEW    = 2;
static const int ACTIVE_COUNCILOR_NOT_CAPABLE      = 3;
static const int ACTIVE_COUNCILOR_STARTED          = 4;

extern CActiveCouncilor activeCouncilor;

// Responsible for activating the Councilor and pinging the network
class CActiveCouncilor
{
public:
    enum councilor_type_enum_t {
        COUNCILOR_UNKNOWN = 0,
        COUNCILOR_REMOTE  = 1,
        COUNCILOR_LOCAL   = 2
    };

private:
    // critical section to protect the inner data structures
    mutable CCriticalSection cs;

    councilor_type_enum_t eType;

    bool fPingerEnabled;

    /// Ping Councilor
    bool SendCouncilorPing(CConnman& connman);

    //  sentinel ping data
    int64_t nSentinelPingTime;
    uint32_t nSentinelVersion;

public:
    // Keys for the active Councilor
    CPubKey pubKeyCouncilor;
    CKey keyCouncilor;

    // Initialized while registering Councilor
    COutPoint outpoint;
    CService service;

    int nState; // should be one of ACTIVE_COUNCILOR_XXXX
    std::string strNotCapableReason;


    CActiveCouncilor()
        : eType(COUNCILOR_UNKNOWN),
          fPingerEnabled(false),
          pubKeyCouncilor(),
          keyCouncilor(),
          outpoint(),
          service(),
          nState(ACTIVE_COUNCILOR_INITIAL)
    {}

    /// Manage state of active Councilor
    void ManageState(CConnman& connman);

    std::string GetStateString() const;
    std::string GetStatus() const;
    std::string GetTypeString() const;

    bool UpdateSentinelPing(int version);

private:
    void ManageStateInitial(CConnman& connman);
    void ManageStateRemote();
    void ManageStateLocal(CConnman& connman);
};

#endif
