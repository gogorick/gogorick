/********************************************************************************
** Form generated from reading UI file 'councilorlist.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COUNCILORLIST_H
#define UI_COUNCILORLIST_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CouncilorList
{
public:
    QVBoxLayout *topLayout;
    QVBoxLayout *verticalLayout;
    QSpacerItem *horizontalSpacer0;
    QTabWidget *tabWidget;
    QWidget *tabMyCouncilors;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_note;
    QLabel *updateNote;
    QTableWidget *tableWidgetMyCouncilors;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *startButton;
    QPushButton *startAllButton;
    QPushButton *startMissingButton;
    QPushButton *UpdateButton;
    QLabel *autoupdate_label;
    QLabel *secondsLabel;
    QSpacerItem *horizontalSpacer_5;
    QWidget *tabAllCouncilors;
    QGridLayout *gridLayout;
    QTableWidget *tableWidgetCouncilors;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_filter;
    QLineEdit *filterLineEdit;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_count;
    QLabel *countLabel;

    void setupUi(QWidget *CouncilorList)
    {
        if (CouncilorList->objectName().isEmpty())
            CouncilorList->setObjectName(QStringLiteral("CouncilorList"));
        CouncilorList->resize(723, 457);
        topLayout = new QVBoxLayout(CouncilorList);
        topLayout->setObjectName(QStringLiteral("topLayout"));
        topLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(-1, -1, -1, 0);
        horizontalSpacer0 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        verticalLayout->addItem(horizontalSpacer0);

        tabWidget = new QTabWidget(CouncilorList);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabMyCouncilors = new QWidget();
        tabMyCouncilors->setObjectName(QStringLiteral("tabMyCouncilors"));
        gridLayout_2 = new QGridLayout(tabMyCouncilors);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, -1, -1, 0);
        horizontalLayout_note = new QHBoxLayout();
        horizontalLayout_note->setObjectName(QStringLiteral("horizontalLayout_note"));
        horizontalLayout_note->setContentsMargins(-1, -1, -1, 0);
        updateNote = new QLabel(tabMyCouncilors);
        updateNote->setObjectName(QStringLiteral("updateNote"));

        horizontalLayout_note->addWidget(updateNote);


        verticalLayout_2->addLayout(horizontalLayout_note);

        tableWidgetMyCouncilors = new QTableWidget(tabMyCouncilors);
        if (tableWidgetMyCouncilors->columnCount() < 7)
            tableWidgetMyCouncilors->setColumnCount(7);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidgetMyCouncilors->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidgetMyCouncilors->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidgetMyCouncilors->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidgetMyCouncilors->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidgetMyCouncilors->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidgetMyCouncilors->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidgetMyCouncilors->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        tableWidgetMyCouncilors->setObjectName(QStringLiteral("tableWidgetMyCouncilors"));
        tableWidgetMyCouncilors->setMinimumSize(QSize(695, 0));
        tableWidgetMyCouncilors->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidgetMyCouncilors->setAlternatingRowColors(true);
        tableWidgetMyCouncilors->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidgetMyCouncilors->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidgetMyCouncilors->setSortingEnabled(true);
        tableWidgetMyCouncilors->horizontalHeader()->setStretchLastSection(true);

        verticalLayout_2->addWidget(tableWidgetMyCouncilors);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(-1, -1, -1, 0);
        startButton = new QPushButton(tabMyCouncilors);
        startButton->setObjectName(QStringLiteral("startButton"));

        horizontalLayout_5->addWidget(startButton);

        startAllButton = new QPushButton(tabMyCouncilors);
        startAllButton->setObjectName(QStringLiteral("startAllButton"));

        horizontalLayout_5->addWidget(startAllButton);

        startMissingButton = new QPushButton(tabMyCouncilors);
        startMissingButton->setObjectName(QStringLiteral("startMissingButton"));

        horizontalLayout_5->addWidget(startMissingButton);

        UpdateButton = new QPushButton(tabMyCouncilors);
        UpdateButton->setObjectName(QStringLiteral("UpdateButton"));

        horizontalLayout_5->addWidget(UpdateButton);

        autoupdate_label = new QLabel(tabMyCouncilors);
        autoupdate_label->setObjectName(QStringLiteral("autoupdate_label"));

        horizontalLayout_5->addWidget(autoupdate_label);

        secondsLabel = new QLabel(tabMyCouncilors);
        secondsLabel->setObjectName(QStringLiteral("secondsLabel"));

        horizontalLayout_5->addWidget(secondsLabel);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);


        verticalLayout_2->addLayout(horizontalLayout_5);


        gridLayout_2->addLayout(verticalLayout_2, 0, 0, 1, 1);

        tabWidget->addTab(tabMyCouncilors, QString());
        tabAllCouncilors = new QWidget();
        tabAllCouncilors->setObjectName(QStringLiteral("tabAllCouncilors"));
        gridLayout = new QGridLayout(tabAllCouncilors);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tableWidgetCouncilors = new QTableWidget(tabAllCouncilors);
        if (tableWidgetCouncilors->columnCount() < 6)
            tableWidgetCouncilors->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidgetCouncilors->setHorizontalHeaderItem(0, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidgetCouncilors->setHorizontalHeaderItem(1, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidgetCouncilors->setHorizontalHeaderItem(2, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidgetCouncilors->setHorizontalHeaderItem(3, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidgetCouncilors->setHorizontalHeaderItem(4, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidgetCouncilors->setHorizontalHeaderItem(5, __qtablewidgetitem12);
        tableWidgetCouncilors->setObjectName(QStringLiteral("tableWidgetCouncilors"));
        tableWidgetCouncilors->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidgetCouncilors->setAlternatingRowColors(true);
        tableWidgetCouncilors->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidgetCouncilors->setSortingEnabled(true);
        tableWidgetCouncilors->horizontalHeader()->setStretchLastSection(true);

        gridLayout->addWidget(tableWidgetCouncilors, 1, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, -1, -1, 0);
        label_filter = new QLabel(tabAllCouncilors);
        label_filter->setObjectName(QStringLiteral("label_filter"));

        horizontalLayout_3->addWidget(label_filter);

        filterLineEdit = new QLineEdit(tabAllCouncilors);
        filterLineEdit->setObjectName(QStringLiteral("filterLineEdit"));

        horizontalLayout_3->addWidget(filterLineEdit);

        horizontalSpacer_3 = new QSpacerItem(10, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        label_count = new QLabel(tabAllCouncilors);
        label_count->setObjectName(QStringLiteral("label_count"));

        horizontalLayout_3->addWidget(label_count);

        countLabel = new QLabel(tabAllCouncilors);
        countLabel->setObjectName(QStringLiteral("countLabel"));

        horizontalLayout_3->addWidget(countLabel);


        gridLayout->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        tabWidget->addTab(tabAllCouncilors, QString());

        verticalLayout->addWidget(tabWidget);


        topLayout->addLayout(verticalLayout);


        retranslateUi(CouncilorList);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CouncilorList);
    } // setupUi

    void retranslateUi(QWidget *CouncilorList)
    {
        CouncilorList->setWindowTitle(QApplication::translate("CouncilorList", "Form", 0));
        updateNote->setText(QApplication::translate("CouncilorList", "Note: Status of your councilors in local wallet can potentially be slightly incorrect.<br />Always wait for wallet to sync additional data and then double check from another node<br />if your councilor should be running but you still do not see \"ENABLED\" in \"Status\" field.", 0));
        QTableWidgetItem *___qtablewidgetitem = tableWidgetMyCouncilors->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("CouncilorList", "Alias", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidgetMyCouncilors->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("CouncilorList", "Address", 0));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidgetMyCouncilors->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("CouncilorList", "Protocol", 0));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidgetMyCouncilors->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("CouncilorList", "Status", 0));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidgetMyCouncilors->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("CouncilorList", "Active", 0));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidgetMyCouncilors->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("CouncilorList", "Last Seen", 0));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidgetMyCouncilors->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("CouncilorList", "Payee", 0));
        startButton->setText(QApplication::translate("CouncilorList", "S&tart alias", 0));
        startAllButton->setText(QApplication::translate("CouncilorList", "Start &all", 0));
        startMissingButton->setText(QApplication::translate("CouncilorList", "Start &MISSING", 0));
        UpdateButton->setText(QApplication::translate("CouncilorList", "&Update status", 0));
        autoupdate_label->setText(QApplication::translate("CouncilorList", "Status will be updated automatically in (sec):", 0));
        secondsLabel->setText(QApplication::translate("CouncilorList", "0", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabMyCouncilors), QApplication::translate("CouncilorList", "My Councilors", 0));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidgetCouncilors->horizontalHeaderItem(0);
        ___qtablewidgetitem7->setText(QApplication::translate("CouncilorList", "Address", 0));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidgetCouncilors->horizontalHeaderItem(1);
        ___qtablewidgetitem8->setText(QApplication::translate("CouncilorList", "Protocol", 0));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidgetCouncilors->horizontalHeaderItem(2);
        ___qtablewidgetitem9->setText(QApplication::translate("CouncilorList", "Status", 0));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidgetCouncilors->horizontalHeaderItem(3);
        ___qtablewidgetitem10->setText(QApplication::translate("CouncilorList", "Active", 0));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidgetCouncilors->horizontalHeaderItem(4);
        ___qtablewidgetitem11->setText(QApplication::translate("CouncilorList", "Last Seen", 0));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidgetCouncilors->horizontalHeaderItem(5);
        ___qtablewidgetitem12->setText(QApplication::translate("CouncilorList", "Payee", 0));
        label_filter->setText(QApplication::translate("CouncilorList", "Filter List:", 0));
#ifndef QT_NO_TOOLTIP
        filterLineEdit->setToolTip(QApplication::translate("CouncilorList", "Filter councilor list", 0));
#endif // QT_NO_TOOLTIP
        label_count->setText(QApplication::translate("CouncilorList", "Node Count:", 0));
        countLabel->setText(QApplication::translate("CouncilorList", "0", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabAllCouncilors), QApplication::translate("CouncilorList", "All Councilors", 0));
    } // retranslateUi

};

namespace Ui {
    class CouncilorList: public Ui_CouncilorList {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COUNCILORLIST_H
