// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "activecouncilor.h"
#include "init.h"
#include "netbase.h"
#include "councilor.h"
#include "councilor-payments.h"
#include "councilor-sync.h"
#include "councilorman.h"
#include "messagesigner.h"
#include "util.h"

#include <boost/lexical_cast.hpp>


CCouncilor::CCouncilor() :
    councilor_info_t{ COUNCILOR_ENABLED, PROTOCOL_VERSION, GetAdjustedTime()},
    fAllowMixingTx(true)
{}

CCouncilor::CCouncilor(CService addr, COutPoint outpoint, CPubKey pubKeyCollateralAddress, CPubKey pubKeyCouncilor, int nProtocolVersionIn) :
    councilor_info_t{ COUNCILOR_ENABLED, nProtocolVersionIn, GetAdjustedTime(),
                       outpoint, addr, pubKeyCollateralAddress, pubKeyCouncilor},
    fAllowMixingTx(true)
{}

CCouncilor::CCouncilor(const CCouncilor& other) :
    councilor_info_t{other},
    lastPing(other.lastPing),
    vchSig(other.vchSig),
    nCollateralMinConfBlockHash(other.nCollateralMinConfBlockHash),
    nBlockLastPaid(other.nBlockLastPaid),
    nPoSeBanScore(other.nPoSeBanScore),
    nPoSeBanHeight(other.nPoSeBanHeight),
    fAllowMixingTx(other.fAllowMixingTx),
    fUnitTest(other.fUnitTest)
{}

CCouncilor::CCouncilor(const CCouncilorBroadcast& mnb) :
    councilor_info_t{ mnb.nActiveState, mnb.nProtocolVersion, mnb.sigTime,
                       mnb.vin.prevout, mnb.addr, mnb.pubKeyCollateralAddress, mnb.pubKeyCouncilor,
                       mnb.sigTime /*nTimeLastWatchdogVote*/},
    lastPing(mnb.lastPing),
    vchSig(mnb.vchSig),
    fAllowMixingTx(true)
{}

//
// When a new councilor broadcast is sent, update our information
//
bool CCouncilor::UpdateFromNewBroadcast(CCouncilorBroadcast& mnb, CConnman& connman)
{
    if(mnb.sigTime <= sigTime && !mnb.fRecovery) return false;

    pubKeyCouncilor = mnb.pubKeyCouncilor;
    sigTime = mnb.sigTime;
    vchSig = mnb.vchSig;
    nProtocolVersion = mnb.nProtocolVersion;
    addr = mnb.addr;
    nPoSeBanScore = 0;
    nPoSeBanHeight = 0;
    nTimeLastChecked = 0;
    int nDos = 0;
    if(mnb.lastPing == CCouncilorPing() || (mnb.lastPing != CCouncilorPing() && mnb.lastPing.CheckAndUpdate(this, true, nDos, connman))) {
        lastPing = mnb.lastPing;
        mnodeman.mapSeenCouncilorPing.insert(std::make_pair(lastPing.GetHash(), lastPing));
    }
    // if it matches our Councilor privkey...
    if(fCounciLor && pubKeyCouncilor == activeCouncilor.pubKeyCouncilor) {
        nPoSeBanScore = -COUNCILOR_POSE_BAN_MAX_SCORE;
        if(nProtocolVersion == PROTOCOL_VERSION) {
            // ... and PROTOCOL_VERSION, then we've been remotely activated ...
            activeCouncilor.ManageState(connman);
        } else {
            // ... otherwise we need to reactivate our node, do not add it to the list and do not relay
            // but also do not ban the node we get this message from
            LogPrintf("CCouncilor::UpdateFromNewBroadcast -- wrong PROTOCOL_VERSION, re-activate your MN: message nProtocolVersion=%d  PROTOCOL_VERSION=%d\n", nProtocolVersion, PROTOCOL_VERSION);
            return false;
        }
    }
    return true;
}

//
// Deterministically calculate a given "score" for a Councilor depending on how close it's hash is to
// the proof of work for that block. The further away they are the better, the furthest will win the election
// and get paid this block
//
arith_uint256 CCouncilor::CalculateScore(const uint256& blockHash)
{
    if (fDIP0001WasLockedIn) {
        // Deterministically calculate a "score" for a Councilor based on any given (block)hash
        CHashWriter ss(SER_GETHASH, PROTOCOL_VERSION);
        ss << vin.prevout << nCollateralMinConfBlockHash << blockHash;
        return UintToArith256(ss.GetHash());
    }

    // TODO: remove calculations below after migration to 12.2

    uint256 aux = ArithToUint256(UintToArith256(vin.prevout.hash) + vin.prevout.n);

    CHashWriter ss(SER_GETHASH, PROTOCOL_VERSION);
    ss << blockHash;
    arith_uint256 hash2 = UintToArith256(ss.GetHash());

    CHashWriter ss2(SER_GETHASH, PROTOCOL_VERSION);
    ss2 << blockHash;
    ss2 << aux;
    arith_uint256 hash3 = UintToArith256(ss2.GetHash());

    return (hash3 > hash2 ? hash3 - hash2 : hash2 - hash3);
}

CCouncilor::CollateralStatus CCouncilor::CheckCollateral(const COutPoint& outpoint)
{
    int nHeight;
    return CheckCollateral(outpoint, nHeight);
}

CCouncilor::CollateralStatus CCouncilor::CheckCollateral(const COutPoint& outpoint, int& nHeightRet)
{
    AssertLockHeld(cs_main);

    CCoins coins;
    if(!GetUTXOCoins(outpoint, coins)) {
        return COLLATERAL_UTXO_NOT_FOUND;
    }

    if(coins.vout[outpoint.n].nValue != 25000 * COIN) {
        return COLLATERAL_INVALID_AMOUNT;
    }

    nHeightRet = coins.nHeight;
    return COLLATERAL_OK;
}

void CCouncilor::Check(bool fForce)
{
    LOCK(cs);

    if(ShutdownRequested()) return;

    if(!fForce && (GetTime() - nTimeLastChecked < COUNCILOR_CHECK_SECONDS)) return;
    nTimeLastChecked = GetTime();

    LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state\n", vin.prevout.ToStringShort(), GetStateString());

    //once spent, stop doing the checks
    if(IsOutpointSpent()) return;

    int nHeight = 0;
    if(!fUnitTest) {
        TRY_LOCK(cs_main, lockMain);
        if(!lockMain) return;

        CollateralStatus err = CheckCollateral(vin.prevout);
        if (err == COLLATERAL_UTXO_NOT_FOUND) {
            nActiveState = COUNCILOR_OUTPOINT_SPENT;
            LogPrint("councilor", "CCouncilor::Check -- Failed to find Councilor UTXO, councilor=%s\n", vin.prevout.ToStringShort());
            return;
        }

        nHeight = chainActive.Height();
    }

    if(IsPoSeBanned()) {
        if(nHeight < nPoSeBanHeight) return; // too early?
        // Otherwise give it a chance to proceed further to do all the usual checks and to change its state.
        // Councilor still will be on the edge and can be banned back easily if it keeps ignoring mnverify
        // or connect attempts. Will require few mnverify messages to strengthen its position in mn list.
        LogPrintf("CCouncilor::Check -- Councilor %s is unbanned and back in list now\n", vin.prevout.ToStringShort());
        DecreasePoSeBanScore();
    } else if(nPoSeBanScore >= COUNCILOR_POSE_BAN_MAX_SCORE) {
        nActiveState = COUNCILOR_POSE_BAN;
        // ban for the whole payment cycle
        nPoSeBanHeight = nHeight + mnodeman.size();
        LogPrintf("CCouncilor::Check -- Councilor %s is banned till block %d now\n", vin.prevout.ToStringShort(), nPoSeBanHeight);
        return;
    }

    int nActiveStatePrev = nActiveState;
    bool fOurCouncilor = fCounciLor && activeCouncilor.pubKeyCouncilor == pubKeyCouncilor;

                   // councilor doesn't meet payment protocol requirements ...
    bool fRequireUpdate = nProtocolVersion < mnpayments.GetMinCouncilorPaymentsProto() ||
                   // or it's our own node and we just updated it to the new protocol but we are still waiting for activation ...
                   (fOurCouncilor && nProtocolVersion < PROTOCOL_VERSION);

    if(fRequireUpdate) {
        nActiveState = COUNCILOR_UPDATE_REQUIRED;
        if(nActiveStatePrev != nActiveState) {
            LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state now\n", vin.prevout.ToStringShort(), GetStateString());
        }
        return;
    }

    // keep old councilors on start, give them a chance to receive updates...
    bool fWaitForPing = !councilorSync.IsCouncilorListSynced() && !IsPingedWithin(COUNCILOR_MIN_MNP_SECONDS);

    if(fWaitForPing && !fOurCouncilor) {
        // ...but if it was already expired before the initial check - return right away
        if(IsExpired() || IsWatchdogExpired() || IsNewStartRequired()) {
            LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state, waiting for ping\n", vin.prevout.ToStringShort(), GetStateString());
            return;
        }
    }

    // don't expire if we are still in "waiting for ping" mode unless it's our own councilor
    if(!fWaitForPing || fOurCouncilor) {

        if(!IsPingedWithin(COUNCILOR_NEW_START_REQUIRED_SECONDS)) {
            nActiveState = COUNCILOR_NEW_START_REQUIRED;
            if(nActiveStatePrev != nActiveState) {
                LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state now\n", vin.prevout.ToStringShort(), GetStateString());
            }
            return;
        }

        bool fWatchdogActive = councilorSync.IsSynced() && mnodeman.IsWatchdogActive();
        bool fWatchdogExpired = (fWatchdogActive && ((GetAdjustedTime() - nTimeLastWatchdogVote) > COUNCILOR_WATCHDOG_MAX_SECONDS));

        LogPrint("councilor", "CCouncilor::Check -- outpoint=%s, nTimeLastWatchdogVote=%d, GetAdjustedTime()=%d, fWatchdogExpired=%d\n",
                vin.prevout.ToStringShort(), nTimeLastWatchdogVote, GetAdjustedTime(), fWatchdogExpired);

        if(fWatchdogExpired) {
            nActiveState = COUNCILOR_WATCHDOG_EXPIRED;
            if(nActiveStatePrev != nActiveState) {
                LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state now\n", vin.prevout.ToStringShort(), GetStateString());
            }
            return;
        }

        if(!IsPingedWithin(COUNCILOR_EXPIRATION_SECONDS)) {
            nActiveState = COUNCILOR_EXPIRED;
            if(nActiveStatePrev != nActiveState) {
                LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state now\n", vin.prevout.ToStringShort(), GetStateString());
            }
            return;
        }
    }

    if(lastPing.sigTime - sigTime < COUNCILOR_MIN_MNP_SECONDS) {
        nActiveState = COUNCILOR_PRE_ENABLED;
        if(nActiveStatePrev != nActiveState) {
            LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state now\n", vin.prevout.ToStringShort(), GetStateString());
        }
        return;
    }

    nActiveState = COUNCILOR_ENABLED; // OK
    if(nActiveStatePrev != nActiveState) {
        LogPrint("councilor", "CCouncilor::Check -- Councilor %s is in %s state now\n", vin.prevout.ToStringShort(), GetStateString());
    }
}

bool CCouncilor::IsInputAssociatedWithPubkey()
{
    CScript payee;
    payee = GetScriptForDestination(pubKeyCollateralAddress.GetID());

    CTransaction tx;
    uint256 hash;
    if(GetTransaction(vin.prevout.hash, tx, Params().GetConsensus(), hash, true)) {
        BOOST_FOREACH(CTxOut out, tx.vout)
            if(out.nValue == 10000*COIN && out.scriptPubKey == payee) return true;
    }

    return false;
}

bool CCouncilor::IsValidNetAddr()
{
    return IsValidNetAddr(addr);
}

bool CCouncilor::IsValidNetAddr(CService addrIn)
{
    // TODO: regtest is fine with any addresses for now,
    // should probably be a bit smarter if one day we start to implement tests for this
    return Params().NetworkIDString() == CBaseChainParams::REGTEST ||
            (addrIn.IsIPv4() && IsReachable(addrIn) && addrIn.IsRoutable());
}

councilor_info_t CCouncilor::GetInfo()
{
    councilor_info_t info{*this};
    info.nTimeLastPing = lastPing.sigTime;
    info.fInfoValid = true;
    return info;
}

std::string CCouncilor::StateToString(int nStateIn)
{
    switch(nStateIn) {
        case COUNCILOR_PRE_ENABLED:            return "PRE_ENABLED";
        case COUNCILOR_ENABLED:                return "ENABLED";
        case COUNCILOR_EXPIRED:                return "EXPIRED";
        case COUNCILOR_OUTPOINT_SPENT:         return "OUTPOINT_SPENT";
        case COUNCILOR_UPDATE_REQUIRED:        return "UPDATE_REQUIRED";
        case COUNCILOR_WATCHDOG_EXPIRED:       return "WATCHDOG_EXPIRED";
        case COUNCILOR_NEW_START_REQUIRED:     return "NEW_START_REQUIRED";
        case COUNCILOR_POSE_BAN:               return "POSE_BAN";
        default:                                return "UNKNOWN";
    }
}

std::string CCouncilor::GetStateString() const
{
    return StateToString(nActiveState);
}

std::string CCouncilor::GetStatus() const
{
    // TODO: return smth a bit more human readable here
    return GetStateString();
}

void CCouncilor::UpdateLastPaid(const CBlockIndex *pindex, int nMaxBlocksToScanBack)
{
    if(!pindex) return;

    const CBlockIndex *BlockReading = pindex;

    CScript mnpayee = GetScriptForDestination(pubKeyCollateralAddress.GetID());
    // LogPrint("councilor", "CCouncilor::UpdateLastPaidBlock -- searching for block with payment to %s\n", vin.prevout.ToStringShort());

    LOCK(cs_mapCouncilorBlocks);

    for (int i = 0; BlockReading && BlockReading->nHeight > nBlockLastPaid && i < nMaxBlocksToScanBack; i++) {
        if(mnpayments.mapCouncilorBlocks.count(BlockReading->nHeight) &&
            mnpayments.mapCouncilorBlocks[BlockReading->nHeight].HasPayeeWithVotes(mnpayee, 2))
        {
            CBlock block;
            if(!ReadBlockFromDisk(block, BlockReading, Params().GetConsensus())) // shouldn't really happen
                continue;

            CAmount nCouncilorPayment = GetCouncilorPayment(BlockReading->nHeight, block.vtx[0].GetValueOut());

            BOOST_FOREACH(CTxOut txout, block.vtx[0].vout)
                if(mnpayee == txout.scriptPubKey && nCouncilorPayment == txout.nValue) {
                    nBlockLastPaid = BlockReading->nHeight;
                    nTimeLastPaid = BlockReading->nTime;
                    LogPrint("councilor", "CCouncilor::UpdateLastPaidBlock -- searching for block with payment to %s -- found new %d\n", vin.prevout.ToStringShort(), nBlockLastPaid);
                    return;
                }
        }

        if (BlockReading->pprev == NULL) { assert(BlockReading); break; }
        BlockReading = BlockReading->pprev;
    }

    // Last payment for this councilor wasn't found in latest mnpayments blocks
    // or it was found in mnpayments blocks but wasn't found in the blockchain.
    // LogPrint("councilor", "CCouncilor::UpdateLastPaidBlock -- searching for block with payment to %s -- keeping old %d\n", vin.prevout.ToStringShort(), nBlockLastPaid);
}

bool CCouncilorBroadcast::Create(std::string strService, std::string strKeyCouncilor, std::string strTxHash, std::string strOutputIndex, std::string& strErrorRet, CCouncilorBroadcast &mnbRet, bool fOffline)
{
    COutPoint outpoint;
    CPubKey pubKeyCollateralAddressNew;
    CKey keyCollateralAddressNew;
    CPubKey pubKeyCouncilorNew;
    CKey keyCouncilorNew;

    auto Log = [&strErrorRet](std::string sErr)->bool
    {
        strErrorRet = sErr;
        LogPrintf("CCouncilorBroadcast::Create -- %s\n", strErrorRet);
        return false;
    };

    //need correct blocks to send ping
    if (!fOffline && !councilorSync.IsBlockchainSynced())
        return Log("Sync in progress. Must wait until sync is complete to start Councilor");

    if (!CMessageSigner::GetKeysFromSecret(strKeyCouncilor, keyCouncilorNew, pubKeyCouncilorNew))
        return Log(strprintf("Invalid councilor key %s", strKeyCouncilor));

    if (!pwalletMain->GetCouncilorOutpointAndKeys(outpoint, pubKeyCollateralAddressNew, keyCollateralAddressNew, strTxHash, strOutputIndex))
        return Log(strprintf("Could not allocate outpoint %s:%s for councilor %s", strTxHash, strOutputIndex, strService));

    CService service;
    if (!Lookup(strService.c_str(), service, 0, false))
        return Log(strprintf("Invalid address %s for councilor.", strService));
    int mainnetDefaultPort = Params(CBaseChainParams::MAIN).GetDefaultPort();
    if (Params().NetworkIDString() == CBaseChainParams::MAIN) {
        if (service.GetPort() != mainnetDefaultPort)
            return Log(strprintf("Invalid port %u for councilor %s, only %d is supported on mainnet.", service.GetPort(), strService, mainnetDefaultPort));
    } else if (service.GetPort() == mainnetDefaultPort)
        return Log(strprintf("Invalid port %u for councilor %s, %d is the only supported on mainnet.", service.GetPort(), strService, mainnetDefaultPort));

    return Create(outpoint, service, keyCollateralAddressNew, pubKeyCollateralAddressNew, keyCouncilorNew, pubKeyCouncilorNew, strErrorRet, mnbRet);
}

bool CCouncilorBroadcast::Create(const COutPoint& outpoint, const CService& service, const CKey& keyCollateralAddressNew, const CPubKey& pubKeyCollateralAddressNew, const CKey& keyCouncilorNew, const CPubKey& pubKeyCouncilorNew, std::string &strErrorRet, CCouncilorBroadcast &mnbRet)
{
    // wait for reindex and/or import to finish
    if (fImporting || fReindex) return false;

    LogPrint("councilor", "CCouncilorBroadcast::Create -- pubKeyCollateralAddressNew = %s, pubKeyCouncilorNew.GetID() = %s\n",
             CBitcoinAddress(pubKeyCollateralAddressNew.GetID()).ToString(),
             pubKeyCouncilorNew.GetID().ToString());

    auto Log = [&strErrorRet,&mnbRet](std::string sErr)->bool
    {
        strErrorRet = sErr;
        LogPrintf("CCouncilorBroadcast::Create -- %s\n", strErrorRet);
        mnbRet = CCouncilorBroadcast();
        return false;
    };

    CCouncilorPing mnp(outpoint);
    if (!mnp.Sign(keyCouncilorNew, pubKeyCouncilorNew))
        return Log(strprintf("Failed to sign ping, councilor=%s", outpoint.ToStringShort()));

    mnbRet = CCouncilorBroadcast(service, outpoint, pubKeyCollateralAddressNew, pubKeyCouncilorNew, PROTOCOL_VERSION);

    if (!mnbRet.IsValidNetAddr())
        return Log(strprintf("Invalid IP address, councilor=%s", outpoint.ToStringShort()));

    mnbRet.lastPing = mnp;
    if (!mnbRet.Sign(keyCollateralAddressNew))
        return Log(strprintf("Failed to sign broadcast, councilor=%s", outpoint.ToStringShort()));

    return true;
}

bool CCouncilorBroadcast::SimpleCheck(int& nDos)
{
    nDos = 0;

    // make sure addr is valid
    if(!IsValidNetAddr()) {
        LogPrintf("CCouncilorBroadcast::SimpleCheck -- Invalid addr, rejected: councilor=%s  addr=%s\n",
                    vin.prevout.ToStringShort(), addr.ToString());
        return false;
    }

    // make sure signature isn't in the future (past is OK)
    if (sigTime > GetAdjustedTime() + 60 * 60) {
        LogPrintf("CCouncilorBroadcast::SimpleCheck -- Signature rejected, too far into the future: councilor=%s\n", vin.prevout.ToStringShort());
        nDos = 1;
        return false;
    }

    // empty ping or incorrect sigTime/unknown blockhash
    if(lastPing == CCouncilorPing() || !lastPing.SimpleCheck(nDos)) {
        // one of us is probably forked or smth, just mark it as expired and check the rest of the rules
        nActiveState = COUNCILOR_EXPIRED;
    }

    if(nProtocolVersion < mnpayments.GetMinCouncilorPaymentsProto()) {
        LogPrintf("CCouncilorBroadcast::SimpleCheck -- ignoring outdated Councilor: councilor=%s  nProtocolVersion=%d\n", vin.prevout.ToStringShort(), nProtocolVersion);
        return false;
    }

    CScript pubkeyScript;
    pubkeyScript = GetScriptForDestination(pubKeyCollateralAddress.GetID());

    if(pubkeyScript.size() != 25) {
        LogPrintf("CCouncilorBroadcast::SimpleCheck -- pubKeyCollateralAddress has the wrong size\n");
        nDos = 100;
        return false;
    }

    CScript pubkeyScript2;
    pubkeyScript2 = GetScriptForDestination(pubKeyCouncilor.GetID());

    if(pubkeyScript2.size() != 25) {
        LogPrintf("CCouncilorBroadcast::SimpleCheck -- pubKeyCouncilor has the wrong size\n");
        nDos = 100;
        return false;
    }

    if(!vin.scriptSig.empty()) {
        LogPrintf("CCouncilorBroadcast::SimpleCheck -- Ignore Not Empty ScriptSig %s\n",vin.ToString());
        nDos = 100;
        return false;
    }

    int mainnetDefaultPort = Params(CBaseChainParams::MAIN).GetDefaultPort();
    if(Params().NetworkIDString() == CBaseChainParams::MAIN) {
        if(addr.GetPort() != mainnetDefaultPort) return false;
    } else if(addr.GetPort() == mainnetDefaultPort) return false;

    return true;
}

bool CCouncilorBroadcast::Update(CCouncilor* pmn, int& nDos, CConnman& connman)
{
    nDos = 0;

    if(pmn->sigTime == sigTime && !fRecovery) {
        // mapSeenCouncilorBroadcast in CCouncilorMan::CheckMnbAndUpdateCouncilorList should filter legit duplicates
        // but this still can happen if we just started, which is ok, just do nothing here.
        return false;
    }

    // this broadcast is older than the one that we already have - it's bad and should never happen
    // unless someone is doing something fishy
    if(pmn->sigTime > sigTime) {
        LogPrintf("CCouncilorBroadcast::Update -- Bad sigTime %d (existing broadcast is at %d) for Councilor %s %s\n",
                      sigTime, pmn->sigTime, vin.prevout.ToStringShort(), addr.ToString());
        return false;
    }

    pmn->Check();

    // councilor is banned by PoSe
    if(pmn->IsPoSeBanned()) {
        LogPrintf("CCouncilorBroadcast::Update -- Banned by PoSe, councilor=%s\n", vin.prevout.ToStringShort());
        return false;
    }

    // IsVnAssociatedWithPubkey is validated once in CheckOutpoint, after that they just need to match
    if(pmn->pubKeyCollateralAddress != pubKeyCollateralAddress) {
        LogPrintf("CCouncilorBroadcast::Update -- Got mismatched pubKeyCollateralAddress and vin\n");
        nDos = 33;
        return false;
    }

    if (!CheckSignature(nDos)) {
        LogPrintf("CCouncilorBroadcast::Update -- CheckSignature() failed, councilor=%s\n", vin.prevout.ToStringShort());
        return false;
    }

    // if ther was no councilor broadcast recently or if it matches our Councilor privkey...
    if(!pmn->IsBroadcastedWithin(COUNCILOR_MIN_MNB_SECONDS) || (fCounciLor && pubKeyCouncilor == activeCouncilor.pubKeyCouncilor)) {
        // take the newest entry
        LogPrintf("CCouncilorBroadcast::Update -- Got UPDATED Councilor entry: addr=%s\n", addr.ToString());
        if(pmn->UpdateFromNewBroadcast(*this, connman)) {
            pmn->Check();
            Relay(connman);
        }
        councilorSync.BumpAssetLastTime("CCouncilorBroadcast::Update");
    }

    return true;
}

bool CCouncilorBroadcast::CheckOutpoint(int& nDos)
{
    // we are a councilor with the same vin (i.e. already activated) and this mnb is ours (matches our Councilor privkey)
    // so nothing to do here for us
    if(fCounciLor && vin.prevout == activeCouncilor.outpoint && pubKeyCouncilor == activeCouncilor.pubKeyCouncilor) {
        return false;
    }

    if (!CheckSignature(nDos)) {
        LogPrintf("CCouncilorBroadcast::CheckOutpoint -- CheckSignature() failed, councilor=%s\n", vin.prevout.ToStringShort());
        return false;
    }

    {
        TRY_LOCK(cs_main, lockMain);
        if(!lockMain) {
            // not mnb fault, let it to be checked again later
            LogPrint("councilor", "CCouncilorBroadcast::CheckOutpoint -- Failed to aquire lock, addr=%s", addr.ToString());
            mnodeman.mapSeenCouncilorBroadcast.erase(GetHash());
            return false;
        }

        int nHeight;
        CollateralStatus err = CheckCollateral(vin.prevout, nHeight);
        if (err == COLLATERAL_UTXO_NOT_FOUND) {
            LogPrint("councilor", "CCouncilorBroadcast::CheckOutpoint -- Failed to find Councilor UTXO, councilor=%s\n", vin.prevout.ToStringShort());
            return false;
        }

        if (err == COLLATERAL_INVALID_AMOUNT) {
            LogPrint("councilor", "CCouncilorBroadcast::CheckOutpoint -- Councilor UTXO should have 25,000 SCHMECKLES, councilor=%s\n", vin.prevout.ToStringShort());
            return false;
        }

        if(chainActive.Height() - nHeight + 1 < Params().GetConsensus().nCouncilorMinimumConfirmations) {
            LogPrintf("CCouncilorBroadcast::CheckOutpoint -- Councilor UTXO must have at least %d confirmations, councilor=%s\n",
                    Params().GetConsensus().nCouncilorMinimumConfirmations, vin.prevout.ToStringShort());
            // maybe we miss few blocks, let this mnb to be checked again later
            mnodeman.mapSeenCouncilorBroadcast.erase(GetHash());
            return false;
        }
        // remember the hash of the block where councilor collateral had minimum required confirmations
        nCollateralMinConfBlockHash = chainActive[nHeight + Params().GetConsensus().nCouncilorMinimumConfirmations - 1]->GetBlockHash();
    }

    LogPrint("councilor", "CCouncilorBroadcast::CheckOutpoint -- Councilor UTXO verified\n");

    // make sure the input that was signed in councilor broadcast message is related to the transaction
    // that spawned the Councilor - this is expensive, so it's only done once per Councilor
    if(!IsInputAssociatedWithPubkey()) {
        LogPrintf("CCouncilorMan::CheckOutpoint -- Got mismatched pubKeyCollateralAddress and vin\n");
        nDos = 33;
        return false;
    }

    // verify that sig time is legit in past
    // should be at least not earlier than block when 25,000 SCHMECKLES tx got nCouncilorMinimumConfirmations
    uint256 hashBlock = uint256();
    CTransaction tx2;
    GetTransaction(vin.prevout.hash, tx2, Params().GetConsensus(), hashBlock, true);
    {
        LOCK(cs_main);
        BlockMap::iterator mi = mapBlockIndex.find(hashBlock);
        if (mi != mapBlockIndex.end() && (*mi).second) {
            CBlockIndex* pMNIndex = (*mi).second; // block for 25000 SCHMECKLES tx -> 1 confirmation
            CBlockIndex* pConfIndex = chainActive[pMNIndex->nHeight + Params().GetConsensus().nCouncilorMinimumConfirmations - 1]; // block where tx got nCouncilorMinimumConfirmations
            if(pConfIndex->GetBlockTime() > sigTime) {
                LogPrintf("CCouncilorBroadcast::CheckOutpoint -- Bad sigTime %d (%d conf block is at %d) for Councilor %s %s\n",
                          sigTime, Params().GetConsensus().nCouncilorMinimumConfirmations, pConfIndex->GetBlockTime(), vin.prevout.ToStringShort(), addr.ToString());
                return false;
            }
        }
    }

    return true;
}

bool CCouncilorBroadcast::Sign(const CKey& keyCollateralAddress)
{
    std::string strError;
    std::string strMessage;

    sigTime = GetAdjustedTime();

    strMessage = addr.ToString(false) + boost::lexical_cast<std::string>(sigTime) +
                    pubKeyCollateralAddress.GetID().ToString() + pubKeyCouncilor.GetID().ToString() +
                    boost::lexical_cast<std::string>(nProtocolVersion);

    if(!CMessageSigner::SignMessage(strMessage, vchSig, keyCollateralAddress)) {
        LogPrintf("CCouncilorBroadcast::Sign -- SignMessage() failed\n");
        return false;
    }

    if(!CMessageSigner::VerifyMessage(pubKeyCollateralAddress, vchSig, strMessage, strError)) {
        LogPrintf("CCouncilorBroadcast::Sign -- VerifyMessage() failed, error: %s\n", strError);
        return false;
    }

    return true;
}

bool CCouncilorBroadcast::CheckSignature(int& nDos)
{
    std::string strMessage;
    std::string strError = "";
    nDos = 0;

    strMessage = addr.ToString(false) + boost::lexical_cast<std::string>(sigTime) +
                    pubKeyCollateralAddress.GetID().ToString() + pubKeyCouncilor.GetID().ToString() +
                    boost::lexical_cast<std::string>(nProtocolVersion);

    LogPrint("councilor", "CCouncilorBroadcast::CheckSignature -- strMessage: %s  pubKeyCollateralAddress address: %s  sig: %s\n", strMessage, CBitcoinAddress(pubKeyCollateralAddress.GetID()).ToString(), EncodeBase64(&vchSig[0], vchSig.size()));

    if(!CMessageSigner::VerifyMessage(pubKeyCollateralAddress, vchSig, strMessage, strError)){
        LogPrintf("CCouncilorBroadcast::CheckSignature -- Got bad Councilor announce signature, error: %s\n", strError);
        nDos = 100;
        return false;
    }

    return true;
}

void CCouncilorBroadcast::Relay(CConnman& connman)
{
    CInv inv(MSG_COUNCILOR_ANNOUNCE, GetHash());
    connman.RelayInv(inv);
}

CCouncilorPing::CCouncilorPing(const COutPoint& outpoint)
{
    LOCK(cs_main);
    if (!chainActive.Tip() || chainActive.Height() < 12) return;

    vin = CTxIn(outpoint);
    blockHash = chainActive[chainActive.Height() - 12]->GetBlockHash();
    sigTime = GetAdjustedTime();
}

bool CCouncilorPing::Sign(const CKey& keyCouncilor, const CPubKey& pubKeyCouncilor)
{
    std::string strError;
    std::string strCounciLorSignMessage;

    // TODO: add sentinel data
    sigTime = GetAdjustedTime();
    std::string strMessage = vin.ToString() + blockHash.ToString() + boost::lexical_cast<std::string>(sigTime);

    if(!CMessageSigner::SignMessage(strMessage, vchSig, keyCouncilor)) {
        LogPrintf("CCouncilorPing::Sign -- SignMessage() failed\n");
        return false;
    }

    if(!CMessageSigner::VerifyMessage(pubKeyCouncilor, vchSig, strMessage, strError)) {
        LogPrintf("CCouncilorPing::Sign -- VerifyMessage() failed, error: %s\n", strError);
        return false;
    }

    return true;
}

bool CCouncilorPing::CheckSignature(CPubKey& pubKeyCouncilor, int &nDos)
{
    // TODO: add sentinel data
    std::string strMessage = vin.ToString() + blockHash.ToString() + boost::lexical_cast<std::string>(sigTime);
    std::string strError = "";
    nDos = 0;

    if(!CMessageSigner::VerifyMessage(pubKeyCouncilor, vchSig, strMessage, strError)) {
        LogPrintf("CCouncilorPing::CheckSignature -- Got bad Councilor ping signature, councilor=%s, error: %s\n", vin.prevout.ToStringShort(), strError);
        nDos = 33;
        return false;
    }
    return true;
}

bool CCouncilorPing::SimpleCheck(int& nDos)
{
    // don't ban by default
    nDos = 0;

    if (sigTime > GetAdjustedTime() + 60 * 60) {
        LogPrintf("CCouncilorPing::SimpleCheck -- Signature rejected, too far into the future, councilor=%s\n", vin.prevout.ToStringShort());
        nDos = 1;
        return false;
    }

    {
        AssertLockHeld(cs_main);
        BlockMap::iterator mi = mapBlockIndex.find(blockHash);
        if (mi == mapBlockIndex.end()) {
            LogPrint("councilor", "CCouncilorPing::SimpleCheck -- Councilor ping is invalid, unknown block hash: councilor=%s blockHash=%s\n", vin.prevout.ToStringShort(), blockHash.ToString());
            // maybe we stuck or forked so we shouldn't ban this node, just fail to accept this ping
            // TODO: or should we also request this block?
            return false;
        }
    }
    LogPrint("councilor", "CCouncilorPing::SimpleCheck -- Councilor ping verified: councilor=%s  blockHash=%s  sigTime=%d\n", vin.prevout.ToStringShort(), blockHash.ToString(), sigTime);
    return true;
}

bool CCouncilorPing::CheckAndUpdate(CCouncilor* pmn, bool fFromNewBroadcast, int& nDos, CConnman& connman)
{
    // don't ban by default
    nDos = 0;

    if (!SimpleCheck(nDos)) {
        return false;
    }

    if (pmn == NULL) {
        LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- Couldn't find Councilor entry, councilor=%s\n", vin.prevout.ToStringShort());
        return false;
    }

    if(!fFromNewBroadcast) {
        if (pmn->IsUpdateRequired()) {
            LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- councilor protocol is outdated, councilor=%s\n", vin.prevout.ToStringShort());
            return false;
        }

        if (pmn->IsNewStartRequired()) {
            LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- councilor is completely expired, new start is required, councilor=%s\n", vin.prevout.ToStringShort());
            return false;
        }
    }

    {
        LOCK(cs_main);
        BlockMap::iterator mi = mapBlockIndex.find(blockHash);
        if ((*mi).second && (*mi).second->nHeight < chainActive.Height() - 24) {
            LogPrintf("CCouncilorPing::CheckAndUpdate -- Councilor ping is invalid, block hash is too old: councilor=%s  blockHash=%s\n", vin.prevout.ToStringShort(), blockHash.ToString());
            // nDos = 1;
            return false;
        }
    }

    LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- New ping: councilor=%s  blockHash=%s  sigTime=%d\n", vin.prevout.ToStringShort(), blockHash.ToString(), sigTime);

    // LogPrintf("mnping - Found corresponding mn for vin: %s\n", vin.prevout.ToStringShort());
    // update only if there is no known ping for this councilor or
    // last ping was more then COUNCILOR_MIN_MNP_SECONDS-60 ago comparing to this one
    if (pmn->IsPingedWithin(COUNCILOR_MIN_MNP_SECONDS - 60, sigTime)) {
        LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- Councilor ping arrived too early, councilor=%s\n", vin.prevout.ToStringShort());
        //nDos = 1; //disable, this is happening frequently and causing banned peers
        return false;
    }

    if (!CheckSignature(pmn->pubKeyCouncilor, nDos)) return false;

    // so, ping seems to be ok

    // if we are still syncing and there was no known ping for this mn for quite a while
    // (NOTE: assuming that COUNCILOR_EXPIRATION_SECONDS/2 should be enough to finish mn list sync)
    if(!councilorSync.IsCouncilorListSynced() && !pmn->IsPingedWithin(COUNCILOR_EXPIRATION_SECONDS/2)) {
        // let's bump sync timeout
        LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- bumping sync timeout, councilor=%s\n", vin.prevout.ToStringShort());
        councilorSync.BumpAssetLastTime("CCouncilorPing::CheckAndUpdate");
    }

    // let's store this ping as the last one
    LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- Councilor ping accepted, councilor=%s\n", vin.prevout.ToStringShort());
    pmn->lastPing = *this;

    // and update mnodeman.mapSeenCouncilorBroadcast.lastPing which is probably outdated
    CCouncilorBroadcast mnb(*pmn);
    uint256 hash = mnb.GetHash();
    if (mnodeman.mapSeenCouncilorBroadcast.count(hash)) {
        mnodeman.mapSeenCouncilorBroadcast[hash].second.lastPing = *this;
    }

    // force update, ignoring cache
    pmn->Check(true);
    // relay ping for nodes in ENABLED/EXPIRED/WATCHDOG_EXPIRED state only, skip everyone else
    if (!pmn->IsEnabled() && !pmn->IsExpired() && !pmn->IsWatchdogExpired()) return false;

    LogPrint("councilor", "CCouncilorPing::CheckAndUpdate -- Councilor ping acceepted and relayed, councilor=%s\n", vin.prevout.ToStringShort());
    Relay(connman);

    return true;
}

void CCouncilorPing::Relay(CConnman& connman)
{
    CInv inv(MSG_COUNCILOR_PING, GetHash());
    connman.RelayInv(inv);
}

void CCouncilor::AddCouncilofRicksVote(uint256 nCouncilofRicksObjectHash)
{
    if(mapCouncilofRicksObjectsVotedOn.count(nCouncilofRicksObjectHash)) {
        mapCouncilofRicksObjectsVotedOn[nCouncilofRicksObjectHash]++;
    } else {
        mapCouncilofRicksObjectsVotedOn.insert(std::make_pair(nCouncilofRicksObjectHash, 1));
    }
}

void CCouncilor::RemoveCouncilofRicksObject(uint256 nCouncilofRicksObjectHash)
{
    std::map<uint256, int>::iterator it = mapCouncilofRicksObjectsVotedOn.find(nCouncilofRicksObjectHash);
    if(it == mapCouncilofRicksObjectsVotedOn.end()) {
        return;
    }
    mapCouncilofRicksObjectsVotedOn.erase(it);
}

void CCouncilor::UpdateWatchdogVoteTime(uint64_t nVoteTime)
{
    LOCK(cs);
    nTimeLastWatchdogVote = (nVoteTime == 0) ? GetAdjustedTime() : nVoteTime;
}

/**
*   FLAG COUNCILOFRICKS ITEMS AS DIRTY
*
*   - When councilor come and go on the network, we must flag the items they voted on to recalc it's cached flags
*
*/
void CCouncilor::FlagCouncilofRicksItemsAsDirty()
{
    std::vector<uint256> vecDirty;
    {
        std::map<uint256, int>::iterator it = mapCouncilofRicksObjectsVotedOn.begin();
        while(it != mapCouncilofRicksObjectsVotedOn.end()) {
            vecDirty.push_back(it->first);
            ++it;
        }
    }
    for(size_t i = 0; i < vecDirty.size(); ++i) {
        mnodeman.AddDirtyCouncilofRicksObjectHash(vecDirty[i]);
    }
}
