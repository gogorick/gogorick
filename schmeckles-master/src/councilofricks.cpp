// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "councilofricks.h"
#include "councilofricks-object.h"
#include "councilofricks-vote.h"
#include "councilofricks-classes.h"
#include "net_processing.h"
#include "councilor.h"
#include "councilor-sync.h"
#include "councilorman.h"
#include "messagesigner.h"
#include "netfulfilledman.h"
#include "util.h"

CCouncilofRicksManager councilofricks;

int nSubmittedFinalBudget;

const std::string CCouncilofRicksManager::SERIALIZATION_VERSION_STRING = "CCouncilofRicksManager-Version-12";
const int CCouncilofRicksManager::MAX_TIME_FUTURE_DEVIATION = 60*60;
const int CCouncilofRicksManager::RELIABLE_PROPAGATION_TIME = 60;

CCouncilofRicksManager::CCouncilofRicksManager()
    : nTimeLastDiff(0),
      nCachedBlockHeight(0),
      mapObjects(),
      mapErasedCouncilofRicksObjects(),
      mapCouncilorOrphanObjects(),
      mapWatchdogObjects(),
      nHashWatchdogCurrent(),
      nTimeWatchdogCurrent(0),
      mapVoteToObject(MAX_CACHE_SIZE),
      mapInvalidVotes(MAX_CACHE_SIZE),
      mapOrphanVotes(MAX_CACHE_SIZE),
      mapLastCouncilorObject(),
      setRequestedObjects(),
      fRateChecksEnabled(true),
      cs()
{}

// Accessors for thread-safe access to maps
bool CCouncilofRicksManager::HaveObjectForHash(uint256 nHash) {
    LOCK(cs);
    return (mapObjects.count(nHash) == 1 || mapPostponedObjects.count(nHash) == 1);
}

bool CCouncilofRicksManager::SerializeObjectForHash(uint256 nHash, CDataStream& ss)
{
    LOCK(cs);
    object_m_it it = mapObjects.find(nHash);
    if (it == mapObjects.end()) {
        it = mapPostponedObjects.find(nHash);
        if (it == mapPostponedObjects.end())
            return false;
    }
    ss << it->second;
    return true;
}

bool CCouncilofRicksManager::HaveVoteForHash(uint256 nHash)
{
    LOCK(cs);

    CCouncilofRicksObject* pGovobj = NULL;
    if(!mapVoteToObject.Get(nHash,pGovobj)) {
        return false;
    }

    if(!pGovobj->GetVoteFile().HasVote(nHash)) {
        return false;
    }
    return true;
}

int CCouncilofRicksManager::GetVoteCount() const
{
    LOCK(cs);
    return (int)mapVoteToObject.GetSize();
}

bool CCouncilofRicksManager::SerializeVoteForHash(uint256 nHash, CDataStream& ss)
{
    LOCK(cs);

    CCouncilofRicksObject* pGovobj = NULL;
    if(!mapVoteToObject.Get(nHash,pGovobj)) {
        return false;
    }

    CCouncilofRicksVote vote;
    if(!pGovobj->GetVoteFile().GetVote(nHash, vote)) {
        return false;
    }

    ss << vote;
    return true;
}

void CCouncilofRicksManager::ProcessMessage(CNode* pfrom, std::string& strCommand, CDataStream& vRecv, CConnman& connman)
{
    // lite mode is not supported
    if(fLiteMode) return;
    if(!councilorSync.IsBlockchainSynced()) return;

    if(pfrom->nVersion < MIN_COUNCILOFRICKS_PEER_PROTO_VERSION) return;

    // ANOTHER USER IS ASKING US TO HELP THEM SYNC COUNCILOFRICKS OBJECT DATA
    if (strCommand == NetMsgType::MNCOUNCILOFRICKSSYNC)
    {

        // Ignore such requests until we are fully synced.
        // We could start processing this after councilor list is synced
        // but this is a heavy one so it's better to finish sync first.
        if (!councilorSync.IsSynced()) return;

        uint256 nProp;
        CBloomFilter filter;

        vRecv >> nProp;

        if(pfrom->nVersion >= COUNCILOFRICKS_FILTER_PROTO_VERSION) {
            vRecv >> filter;
            filter.UpdateEmptyFull();
        }
        else {
            filter.clear();
        }

        if(nProp == uint256()) {
            if(netfulfilledman.HasFulfilledRequest(pfrom->addr, NetMsgType::MNCOUNCILOFRICKSSYNC)) {
                // Asking for the whole list multiple times in a short period of time is no good
                LogPrint("gobject", "MNCOUNCILOFRICKSSYNC -- peer already asked me for the list\n");
                Misbehaving(pfrom->GetId(), 20);
                return;
            }
            netfulfilledman.AddFulfilledRequest(pfrom->addr, NetMsgType::MNCOUNCILOFRICKSSYNC);
        }

        Sync(pfrom, nProp, filter, connman);
        LogPrint("gobject", "MNCOUNCILOFRICKSSYNC -- syncing councilofricks objects to our peer at %s\n", pfrom->addr.ToString());

    }

    // A NEW COUNCILOFRICKS OBJECT HAS ARRIVED
    else if (strCommand == NetMsgType::MNCOUNCILOFRICKSOBJECT)
    {
        // MAKE SURE WE HAVE A VALID REFERENCE TO THE TIP BEFORE CONTINUING


        if(!councilorSync.IsCouncilorListSynced()) {
            LogPrint("gobject", "MNCOUNCILOFRICKSOBJECT -- councilor list not synced\n");
            return;
        }

        CCouncilofRicksObject govobj;
        vRecv >> govobj;

        uint256 nHash = govobj.GetHash();
        std::string strHash = nHash.ToString();

        pfrom->setAskFor.erase(nHash);

        LogPrint("gobject", "MNCOUNCILOFRICKSOBJECT -- Received object: %s\n", strHash);

        if(!AcceptObjectMessage(nHash)) {
            LogPrintf("MNCOUNCILOFRICKSOBJECT -- Received unrequested object: %s\n", strHash);
            return;
        }

        LOCK2(cs_main, cs);

        if(mapObjects.count(nHash) || mapPostponedObjects.count(nHash) ||
           mapErasedCouncilofRicksObjects.count(nHash) || mapCouncilorOrphanObjects.count(nHash)) {
            // TODO - print error code? what if it's GOVOBJ_ERROR_IMMATURE?
            LogPrint("gobject", "MNCOUNCILOFRICKSOBJECT -- Received already seen object: %s\n", strHash);
            return;
        }

        bool fRateCheckBypassed = false;
        if(!CouncilorRateCheck(govobj, true, false, fRateCheckBypassed)) {
            LogPrintf("MNCOUNCILOFRICKSOBJECT -- councilor rate check failed - %s - (current block height %d) \n", strHash, nCachedBlockHeight);
            return;
        }

        std::string strError = "";
        // CHECK OBJECT AGAINST LOCAL BLOCKCHAIN

        bool fCouncilorMissing = false;
        bool fMissingConfirmations = false;
        bool fIsValid = govobj.IsValidLocally(strError, fCouncilorMissing, fMissingConfirmations, true);

        if(fRateCheckBypassed && (fIsValid || fCouncilorMissing)) {
            if(!CouncilorRateCheck(govobj, true)) {
                LogPrintf("MNCOUNCILOFRICKSOBJECT -- councilor rate check failed (after signature verification) - %s - (current block height %d) \n", strHash, nCachedBlockHeight);
                return;
            }
        }

        if(!fIsValid) {
            if(fCouncilorMissing) {

                int& count = mapCouncilorOrphanCounter[govobj.GetCouncilorVin().prevout];
                if (count >= 10) {
                    LogPrint("gobject", "MNCOUNCILOFRICKSOBJECT -- Too many orphan objects, missing councilor=%s\n", govobj.GetCouncilorVin().prevout.ToStringShort());
                    // ask for this object again in 2 minutes
                    CInv inv(MSG_COUNCILOFRICKS_OBJECT, govobj.GetHash());
                    pfrom->AskFor(inv);
                    return;
                }

                count++;
                ExpirationInfo info(pfrom->GetId(), GetAdjustedTime() + COUNCILOFRICKS_ORPHAN_EXPIRATION_TIME);
                mapCouncilorOrphanObjects.insert(std::make_pair(nHash, object_info_pair_t(govobj, info)));
                LogPrintf("MNCOUNCILOFRICKSOBJECT -- Missing councilor for: %s, strError = %s\n", strHash, strError);
            } else if(fMissingConfirmations) {
                AddPostponedObject(govobj);
                LogPrintf("MNCOUNCILOFRICKSOBJECT -- Not enough fee confirmations for: %s, strError = %s\n", strHash, strError);
            } else {
                LogPrintf("MNCOUNCILOFRICKSOBJECT -- CouncilofRicks object is invalid - %s\n", strError);
                // apply node's ban score
                Misbehaving(pfrom->GetId(), 20);
            }

            return;
        }

        AddCouncilofRicksObject(govobj, connman, pfrom);
    }

    // A NEW COUNCILOFRICKS OBJECT VOTE HAS ARRIVED
    else if (strCommand == NetMsgType::MNCOUNCILOFRICKSOBJECTVOTE)
    {
        // Ignore such messages until councilor list is synced
        if(!councilorSync.IsCouncilorListSynced()) {
            LogPrint("gobject", "MNCOUNCILOFRICKSOBJECTVOTE -- councilor list not synced\n");
            return;
        }

        CCouncilofRicksVote vote;
        vRecv >> vote;

        LogPrint("gobject", "MNCOUNCILOFRICKSOBJECTVOTE -- Received vote: %s\n", vote.ToString());

        uint256 nHash = vote.GetHash();
        std::string strHash = nHash.ToString();

        pfrom->setAskFor.erase(nHash);

        if(!AcceptVoteMessage(nHash)) {
            LogPrint("gobject", "MNCOUNCILOFRICKSOBJECTVOTE -- Received unrequested vote object: %s, hash: %s, peer = %d\n",
                      vote.ToString(), strHash, pfrom->GetId());
            return;
        }

        CCouncilofRicksException exception;
        if(ProcessVote(pfrom, vote, exception, connman)) {
            LogPrint("gobject", "MNCOUNCILOFRICKSOBJECTVOTE -- %s new\n", strHash);
            councilorSync.BumpAssetLastTime("MNCOUNCILOFRICKSOBJECTVOTE");
            vote.Relay(connman);
        }
        else {
            LogPrint("gobject", "MNCOUNCILOFRICKSOBJECTVOTE -- Rejected vote, error = %s\n", exception.what());
            if((exception.GetNodePenalty() != 0) && councilorSync.IsSynced()) {
                Misbehaving(pfrom->GetId(), exception.GetNodePenalty());
            }
            return;
        }

    }
}

void CCouncilofRicksManager::CheckOrphanVotes(CCouncilofRicksObject& govobj, CCouncilofRicksException& exception, CConnman& connman)
{
    uint256 nHash = govobj.GetHash();
    std::vector<vote_time_pair_t> vecVotePairs;
    mapOrphanVotes.GetAll(nHash, vecVotePairs);

    ScopedLockBool guard(cs, fRateChecksEnabled, false);

    int64_t nNow = GetAdjustedTime();
    for(size_t i = 0; i < vecVotePairs.size(); ++i) {
        bool fRemove = false;
        vote_time_pair_t& pairVote = vecVotePairs[i];
        CCouncilofRicksVote& vote = pairVote.first;
        CCouncilofRicksException exception;
        if(pairVote.second < nNow) {
            fRemove = true;
        }
        else if(govobj.ProcessVote(NULL, vote, exception, connman)) {
            vote.Relay(connman);
            fRemove = true;
        }
        if(fRemove) {
            mapOrphanVotes.Erase(nHash, pairVote);
        }
    }
}

void CCouncilofRicksManager::AddCouncilofRicksObject(CCouncilofRicksObject& govobj, CConnman& connman, CNode* pfrom)
{
    DBG( cout << "CCouncilofRicksManager::AddCouncilofRicksObject START" << endl; );

    uint256 nHash = govobj.GetHash();
    std::string strHash = nHash.ToString();

    // UPDATE CACHED VARIABLES FOR THIS OBJECT AND ADD IT TO OUR MANANGED DATA

    govobj.UpdateSentinelVariables(); //this sets local vars in object

    LOCK2(cs_main, cs);
    std::string strError = "";

    // MAKE SURE THIS OBJECT IS OK

    if(!govobj.IsValidLocally(strError, true)) {
        LogPrintf("CCouncilofRicksManager::AddCouncilofRicksObject -- invalid councilofricks object - %s - (nCachedBlockHeight %d) \n", strError, nCachedBlockHeight);
        return;
    }

    // IF WE HAVE THIS OBJECT ALREADY, WE DON'T WANT ANOTHER COPY

    if(mapObjects.count(nHash)) {
        LogPrintf("CCouncilofRicksManager::AddCouncilofRicksObject -- already have councilofricks object %s\n", nHash.ToString());
        return;
    }

    LogPrint("gobject", "CCouncilofRicksManager::AddCouncilofRicksObject -- Adding object: hash = %s, type = %d\n", nHash.ToString(), govobj.GetObjectType());

    if(govobj.nObjectType == COUNCILOFRICKS_OBJECT_WATCHDOG) {
        // If it's a watchdog, make sure it fits required time bounds
        if((govobj.GetCreationTime() < GetAdjustedTime() - COUNCILOFRICKS_WATCHDOG_EXPIRATION_TIME ||
            govobj.GetCreationTime() > GetAdjustedTime() + COUNCILOFRICKS_WATCHDOG_EXPIRATION_TIME)
            ) {
            // drop it
            LogPrint("gobject", "CCouncilofRicksManager::AddCouncilofRicksObject -- CreationTime is out of bounds: hash = %s\n", nHash.ToString());
            return;
        }

        if(!UpdateCurrentWatchdog(govobj)) {
            // Allow wd's which are not current to be reprocessed
            if(pfrom && (nHashWatchdogCurrent != uint256())) {
                pfrom->PushInventory(CInv(MSG_COUNCILOFRICKS_OBJECT, nHashWatchdogCurrent));
            }
            LogPrint("gobject", "CCouncilofRicksManager::AddCouncilofRicksObject -- Watchdog not better than current: hash = %s\n", nHash.ToString());
            return;
        }
    }

    // INSERT INTO OUR COUNCILOFRICKS OBJECT MEMORY
    mapObjects.insert(std::make_pair(nHash, govobj));

    // SHOULD WE ADD THIS OBJECT TO ANY OTHER MANANGERS?

    DBG( cout << "CCouncilofRicksManager::AddCouncilofRicksObject Before trigger block, strData = "
              << govobj.GetDataAsString()
              << ", nObjectType = " << govobj.nObjectType
              << endl; );

    switch(govobj.nObjectType) {
    case COUNCILOFRICKS_OBJECT_TRIGGER:
        DBG( cout << "CCouncilofRicksManager::AddCouncilofRicksObject Before AddNewTrigger" << endl; );
        triggerman.AddNewTrigger(nHash);
        DBG( cout << "CCouncilofRicksManager::AddCouncilofRicksObject After AddNewTrigger" << endl; );
        break;
    case COUNCILOFRICKS_OBJECT_WATCHDOG:
        mapWatchdogObjects[nHash] = govobj.GetCreationTime() + COUNCILOFRICKS_WATCHDOG_EXPIRATION_TIME;
        LogPrint("gobject", "CCouncilofRicksManager::AddCouncilofRicksObject -- Added watchdog to map: hash = %s\n", nHash.ToString());
        break;
    default:
        break;
    }

    LogPrintf("AddCouncilofRicksObject -- %s new, received form %s\n", strHash, pfrom? pfrom->addrName : "NULL");
    govobj.Relay(connman);

    // Update the rate buffer
    CouncilorRateUpdate(govobj);

    councilorSync.BumpAssetLastTime("CCouncilofRicksManager::AddCouncilofRicksObject");

    // WE MIGHT HAVE PENDING/ORPHAN VOTES FOR THIS OBJECT

    CCouncilofRicksException exception;
    CheckOrphanVotes(govobj, exception, connman);

    DBG( cout << "CCouncilofRicksManager::AddCouncilofRicksObject END" << endl; );
}

bool CCouncilofRicksManager::UpdateCurrentWatchdog(CCouncilofRicksObject& watchdogNew)
{
    bool fAccept = false;

    arith_uint256 nHashNew = UintToArith256(watchdogNew.GetHash());
    arith_uint256 nHashCurrent = UintToArith256(nHashWatchdogCurrent);

    int64_t nExpirationDelay = COUNCILOFRICKS_WATCHDOG_EXPIRATION_TIME / 2;
    int64_t nNow = GetAdjustedTime();

    if(nHashWatchdogCurrent == uint256() ||                                             // no known current OR
       ((nNow - watchdogNew.GetCreationTime() < nExpirationDelay) &&                    // (new one is NOT expired AND
        ((nNow - nTimeWatchdogCurrent > nExpirationDelay) || (nHashNew > nHashCurrent)))//  (current is expired OR
                                                                                        //   its hash is lower))
    ) {
        LOCK(cs);
        object_m_it it = mapObjects.find(nHashWatchdogCurrent);
        if(it != mapObjects.end()) {
            LogPrint("gobject", "CCouncilofRicksManager::UpdateCurrentWatchdog -- Expiring previous current watchdog, hash = %s\n", nHashWatchdogCurrent.ToString());
            it->second.fExpired = true;
            if(it->second.nDeletionTime == 0) {
                it->second.nDeletionTime = nNow;
            }
        }
        nHashWatchdogCurrent = watchdogNew.GetHash();
        nTimeWatchdogCurrent = watchdogNew.GetCreationTime();
        fAccept = true;
        LogPrint("gobject", "CCouncilofRicksManager::UpdateCurrentWatchdog -- Current watchdog updated to: hash = %s\n",
                 ArithToUint256(nHashNew).ToString());
    }

    return fAccept;
}

void CCouncilofRicksManager::UpdateCachesAndClean()
{
    LogPrint("gobject", "CCouncilofRicksManager::UpdateCachesAndClean\n");

    std::vector<uint256> vecDirtyHashes = mnodeman.GetAndClearDirtyCouncilofRicksObjectHashes();

    LOCK2(cs_main, cs);

    // Flag expired watchdogs for removal
    int64_t nNow = GetAdjustedTime();
    LogPrint("gobject", "CCouncilofRicksManager::UpdateCachesAndClean -- Number watchdogs in map: %d, current time = %d\n", mapWatchdogObjects.size(), nNow);
    if(mapWatchdogObjects.size() > 1) {
        hash_time_m_it it = mapWatchdogObjects.begin();
        while(it != mapWatchdogObjects.end()) {
            LogPrint("gobject", "CCouncilofRicksManager::UpdateCachesAndClean -- Checking watchdog: %s, expiration time = %d\n", it->first.ToString(), it->second);
            if(it->second < nNow) {
                LogPrint("gobject", "CCouncilofRicksManager::UpdateCachesAndClean -- Attempting to expire watchdog: %s, expiration time = %d\n", it->first.ToString(), it->second);
                object_m_it it2 = mapObjects.find(it->first);
                if(it2 != mapObjects.end()) {
                    LogPrint("gobject", "CCouncilofRicksManager::UpdateCachesAndClean -- Expiring watchdog: %s, expiration time = %d\n", it->first.ToString(), it->second);
                    it2->second.fExpired = true;
                    if(it2->second.nDeletionTime == 0) {
                        it2->second.nDeletionTime = nNow;
                    }
                }
                if(it->first == nHashWatchdogCurrent) {
                    nHashWatchdogCurrent = uint256();
                }
                mapWatchdogObjects.erase(it++);
            }
            else {
                ++it;
            }
        }
    }

    for(size_t i = 0; i < vecDirtyHashes.size(); ++i) {
        object_m_it it = mapObjects.find(vecDirtyHashes[i]);
        if(it == mapObjects.end()) {
            continue;
        }
        it->second.ClearCouncilorVotes();
        it->second.fDirtyCache = true;
    }

    ScopedLockBool guard(cs, fRateChecksEnabled, false);

    // UPDATE CACHE FOR EACH OBJECT THAT IS FLAGGED DIRTYCACHE=TRUE

    object_m_it it = mapObjects.begin();

    // Clean up any expired or invalid triggers
    triggerman.CleanAndRemove();

    while(it != mapObjects.end())
    {
        CCouncilofRicksObject* pObj = &((*it).second);

        if(!pObj) {
            ++it;
            continue;
        }

        uint256 nHash = it->first;
        std::string strHash = nHash.ToString();

        // IF CACHE IS NOT DIRTY, WHY DO THIS?
        if(pObj->IsSetDirtyCache()) {
            // UPDATE LOCAL VALIDITY AGAINST CRYPTO DATA
            pObj->UpdateLocalValidity();

            // UPDATE SENTINEL SIGNALING VARIABLES
            pObj->UpdateSentinelVariables();
        }

        if(pObj->IsSetCachedDelete() && (nHash == nHashWatchdogCurrent)) {
            nHashWatchdogCurrent = uint256();
        }

        // IF DELETE=TRUE, THEN CLEAN THE MESS UP!

        int64_t nTimeSinceDeletion = GetAdjustedTime() - pObj->GetDeletionTime();

        LogPrint("gobject", "CCouncilofRicksManager::UpdateCachesAndClean -- Checking object for deletion: %s, deletion time = %d, time since deletion = %d, delete flag = %d, expired flag = %d\n",
                 strHash, pObj->GetDeletionTime(), nTimeSinceDeletion, pObj->IsSetCachedDelete(), pObj->IsSetExpired());

        if((pObj->IsSetCachedDelete() || pObj->IsSetExpired()) &&
           (nTimeSinceDeletion >= COUNCILOFRICKS_DELETION_DELAY)) {
            LogPrintf("CCouncilofRicksManager::UpdateCachesAndClean -- erase obj %s\n", (*it).first.ToString());
            mnodeman.RemoveCouncilofRicksObject(pObj->GetHash());

            // Remove vote references
            const object_ref_cache_t::list_t& listItems = mapVoteToObject.GetItemList();
            object_ref_cache_t::list_cit lit = listItems.begin();
            while(lit != listItems.end()) {
                if(lit->value == pObj) {
                    uint256 nKey = lit->key;
                    ++lit;
                    mapVoteToObject.Erase(nKey);
                }
                else {
                    ++lit;
                }
            }

            int64_t nSuperblockCycleSeconds = Params().GetConsensus().nSuperblockCycle * Params().GetConsensus().nPowTargetSpacing;
            int64_t nTimeExpired = pObj->GetCreationTime() + 2 * nSuperblockCycleSeconds + COUNCILOFRICKS_DELETION_DELAY;

            if(pObj->GetObjectType() == COUNCILOFRICKS_OBJECT_WATCHDOG) {
                mapWatchdogObjects.erase(nHash);
            } else if(pObj->GetObjectType() != COUNCILOFRICKS_OBJECT_TRIGGER) {
                // keep hashes of deleted proposals forever
                nTimeExpired = std::numeric_limits<int64_t>::max();
            }

            mapErasedCouncilofRicksObjects.insert(std::make_pair(nHash, nTimeExpired));
            mapObjects.erase(it++);
        } else {
            ++it;
        }
    }

    // forget about expired deleted objects
    hash_time_m_it s_it = mapErasedCouncilofRicksObjects.begin();
    while(s_it != mapErasedCouncilofRicksObjects.end()) {
        if(s_it->second < nNow)
            mapErasedCouncilofRicksObjects.erase(s_it++);
        else
            ++s_it;
    }

    LogPrintf("CCouncilofRicksManager::UpdateCachesAndClean -- %s\n", ToString());
}

CCouncilofRicksObject *CCouncilofRicksManager::FindCouncilofRicksObject(const uint256& nHash)
{
    LOCK(cs);

    if(mapObjects.count(nHash))
        return &mapObjects[nHash];

    return NULL;
}

std::vector<CCouncilofRicksVote> CCouncilofRicksManager::GetMatchingVotes(const uint256& nParentHash)
{
    LOCK(cs);
    std::vector<CCouncilofRicksVote> vecResult;

    object_m_it it = mapObjects.find(nParentHash);
    if(it == mapObjects.end()) {
        return vecResult;
    }
    CCouncilofRicksObject& govobj = it->second;

    return govobj.GetVoteFile().GetVotes();
}

std::vector<CCouncilofRicksVote> CCouncilofRicksManager::GetCurrentVotes(const uint256& nParentHash, const COutPoint& mnCollateralOutpointFilter)
{
    LOCK(cs);
    std::vector<CCouncilofRicksVote> vecResult;

    // Find the councilofricks object or short-circuit.
    object_m_it it = mapObjects.find(nParentHash);
    if(it == mapObjects.end()) return vecResult;
    CCouncilofRicksObject& govobj = it->second;

    CCouncilor mn;
    std::map<COutPoint, CCouncilor> mapCouncilors;
    if(mnCollateralOutpointFilter == COutPoint()) {
        mapCouncilors = mnodeman.GetFullCouncilorMap();
    } else if (mnodeman.Get(mnCollateralOutpointFilter, mn)) {
        mapCouncilors[mnCollateralOutpointFilter] = mn;
    }

    // Loop thru each MN collateral outpoint and get the votes for the `nParentHash` councilofricks object
    for (auto& mnpair : mapCouncilors)
    {
        // get a vote_rec_t from the govobj
        vote_rec_t voteRecord;
        if (!govobj.GetCurrentMNVotes(mnpair.first, voteRecord)) continue;

        for (vote_instance_m_it it3 = voteRecord.mapInstances.begin(); it3 != voteRecord.mapInstances.end(); ++it3) {
            int signal = (it3->first);
            int outcome = ((it3->second).eOutcome);
            int64_t nCreationTime = ((it3->second).nCreationTime);

            CCouncilofRicksVote vote = CCouncilofRicksVote(mnpair.first, nParentHash, (vote_signal_enum_t)signal, (vote_outcome_enum_t)outcome);
            vote.SetTime(nCreationTime);

            vecResult.push_back(vote);
        }
    }

    return vecResult;
}

std::vector<CCouncilofRicksObject*> CCouncilofRicksManager::GetAllNewerThan(int64_t nMoreThanTime)
{
    LOCK(cs);

    std::vector<CCouncilofRicksObject*> vGovObjs;

    object_m_it it = mapObjects.begin();
    while(it != mapObjects.end())
    {
        // IF THIS OBJECT IS OLDER THAN TIME, CONTINUE

        if((*it).second.GetCreationTime() < nMoreThanTime) {
            ++it;
            continue;
        }

        // ADD COUNCILOFRICKS OBJECT TO LIST

        CCouncilofRicksObject* pGovObj = &((*it).second);
        vGovObjs.push_back(pGovObj);

        // NEXT

        ++it;
    }

    return vGovObjs;
}

//
// Sort by votes, if there's a tie sort by their feeHash TX
//
struct sortProposalsByVotes {
    bool operator()(const std::pair<CCouncilofRicksObject*, int> &left, const std::pair<CCouncilofRicksObject*, int> &right) {
        if (left.second != right.second)
            return (left.second > right.second);
        return (UintToArith256(left.first->GetCollateralHash()) > UintToArith256(right.first->GetCollateralHash()));
    }
};

void CCouncilofRicksManager::DoMaintenance(CConnman& connman)
{
    if(fLiteMode || !councilorSync.IsSynced()) return;

    // CHECK OBJECTS WE'VE ASKED FOR, REMOVE OLD ENTRIES

    CleanOrphanObjects();

    RequestOrphanObjects(connman);

    // CHECK AND REMOVE - REPROCESS COUNCILOFRICKS OBJECTS

    UpdateCachesAndClean();
}

bool CCouncilofRicksManager::ConfirmInventoryRequest(const CInv& inv)
{
    // do not request objects until it's time to sync
    if(!councilorSync.IsWinnersListSynced()) return false;

    LOCK(cs);

    LogPrint("gobject", "CCouncilofRicksManager::ConfirmInventoryRequest inv = %s\n", inv.ToString());

    // First check if we've already recorded this object
    switch(inv.type) {
    case MSG_COUNCILOFRICKS_OBJECT:
    {
        if(mapObjects.count(inv.hash) == 1 || mapPostponedObjects.count(inv.hash) == 1) {
            LogPrint("gobject", "CCouncilofRicksManager::ConfirmInventoryRequest already have councilofricks object, returning false\n");
            return false;
        }
    }
    break;
    case MSG_COUNCILOFRICKS_OBJECT_VOTE:
    {
        if(mapVoteToObject.HasKey(inv.hash)) {
            LogPrint("gobject", "CCouncilofRicksManager::ConfirmInventoryRequest already have councilofricks vote, returning false\n");
            return false;
        }
    }
    break;
    default:
        LogPrint("gobject", "CCouncilofRicksManager::ConfirmInventoryRequest unknown type, returning false\n");
        return false;
    }


    hash_s_t* setHash = NULL;
    switch(inv.type) {
    case MSG_COUNCILOFRICKS_OBJECT:
        setHash = &setRequestedObjects;
        break;
    case MSG_COUNCILOFRICKS_OBJECT_VOTE:
        setHash = &setRequestedVotes;
        break;
    default:
        return false;
    }

    hash_s_cit it = setHash->find(inv.hash);
    if(it == setHash->end()) {
        setHash->insert(inv.hash);
        LogPrint("gobject", "CCouncilofRicksManager::ConfirmInventoryRequest added inv to requested set\n");
    }

    LogPrint("gobject", "CCouncilofRicksManager::ConfirmInventoryRequest reached end, returning true\n");
    return true;
}

void CCouncilofRicksManager::Sync(CNode* pfrom, const uint256& nProp, const CBloomFilter& filter, CConnman& connman)
{

    /*
        This code checks each of the hash maps for all known budget proposals and finalized budget proposals, then checks them against the
        budget object to see if they're OK. If all checks pass, we'll send it to the peer.
    */

    // do not provide any data until our node is synced
    if(fCounciLor && !councilorSync.IsSynced()) return;

    int nObjCount = 0;
    int nVoteCount = 0;

    // SYNC COUNCILOFRICKS OBJECTS WITH OTHER CLIENT

    LogPrint("gobject", "CCouncilofRicksManager::Sync -- syncing to peer=%d, nProp = %s\n", pfrom->id, nProp.ToString());

    {
        LOCK2(cs_main, cs);

        if(nProp == uint256()) {
            // all valid objects, no votes
            for(object_m_it it = mapObjects.begin(); it != mapObjects.end(); ++it) {
                CCouncilofRicksObject& govobj = it->second;
                std::string strHash = it->first.ToString();

                LogPrint("gobject", "CCouncilofRicksManager::Sync -- attempting to sync govobj: %s, peer=%d\n", strHash, pfrom->id);

                if(govobj.IsSetCachedDelete() || govobj.IsSetExpired()) {
                    LogPrintf("CCouncilofRicksManager::Sync -- not syncing deleted/expired govobj: %s, peer=%d\n",
                              strHash, pfrom->id);
                    continue;
                }

                // Push the inventory budget proposal message over to the other client
                LogPrint("gobject", "CCouncilofRicksManager::Sync -- syncing govobj: %s, peer=%d\n", strHash, pfrom->id);
                pfrom->PushInventory(CInv(MSG_COUNCILOFRICKS_OBJECT, it->first));
                ++nObjCount;
            }
        } else {
            // single valid object and its valid votes
            object_m_it it = mapObjects.find(nProp);
            if(it == mapObjects.end()) {
                LogPrint("gobject", "CCouncilofRicksManager::Sync -- no matching object for hash %s, peer=%d\n", nProp.ToString(), pfrom->id);
                return;
            }
            CCouncilofRicksObject& govobj = it->second;
            std::string strHash = it->first.ToString();

            LogPrint("gobject", "CCouncilofRicksManager::Sync -- attempting to sync govobj: %s, peer=%d\n", strHash, pfrom->id);

            if(govobj.IsSetCachedDelete() || govobj.IsSetExpired()) {
                LogPrintf("CCouncilofRicksManager::Sync -- not syncing deleted/expired govobj: %s, peer=%d\n",
                          strHash, pfrom->id);
                return;
            }

            // Push the inventory budget proposal message over to the other client
            LogPrint("gobject", "CCouncilofRicksManager::Sync -- syncing govobj: %s, peer=%d\n", strHash, pfrom->id);
            pfrom->PushInventory(CInv(MSG_COUNCILOFRICKS_OBJECT, it->first));
            ++nObjCount;

            std::vector<CCouncilofRicksVote> vecVotes = govobj.GetVoteFile().GetVotes();
            for(size_t i = 0; i < vecVotes.size(); ++i) {
                if(filter.contains(vecVotes[i].GetHash())) {
                    continue;
                }
                if(!vecVotes[i].IsValid(true)) {
                    continue;
                }
                pfrom->PushInventory(CInv(MSG_COUNCILOFRICKS_OBJECT_VOTE, vecVotes[i].GetHash()));
                ++nVoteCount;
            }
        }
    }

    connman.PushMessage(pfrom, NetMsgType::SYNCSTATUSCOUNT, COUNCILOR_SYNC_GOVOBJ, nObjCount);
    connman.PushMessage(pfrom, NetMsgType::SYNCSTATUSCOUNT, COUNCILOR_SYNC_GOVOBJ_VOTE, nVoteCount);
    LogPrintf("CCouncilofRicksManager::Sync -- sent %d objects and %d votes to peer=%d\n", nObjCount, nVoteCount, pfrom->id);
}


void CCouncilofRicksManager::CouncilorRateUpdate(const CCouncilofRicksObject& govobj)
{
    int nObjectType = govobj.GetObjectType();
    if((nObjectType != COUNCILOFRICKS_OBJECT_TRIGGER) && (nObjectType != COUNCILOFRICKS_OBJECT_WATCHDOG))
        return;

    const CTxIn& vin = govobj.GetCouncilorVin();
    txout_m_it it  = mapLastCouncilorObject.find(vin.prevout);

    if(it == mapLastCouncilorObject.end())
        it = mapLastCouncilorObject.insert(txout_m_t::value_type(vin.prevout, last_object_rec(true))).first;

    int64_t nTimestamp = govobj.GetCreationTime();
    if (COUNCILOFRICKS_OBJECT_TRIGGER == nObjectType)
        it->second.triggerBuffer.AddTimestamp(nTimestamp);
    else if (COUNCILOFRICKS_OBJECT_WATCHDOG == nObjectType)
        it->second.watchdogBuffer.AddTimestamp(nTimestamp);

    if (nTimestamp > GetTime() + MAX_TIME_FUTURE_DEVIATION - RELIABLE_PROPAGATION_TIME) {
        // schedule additional relay for the object
        setAdditionalRelayObjects.insert(govobj.GetHash());
    }

    it->second.fStatusOK = true;
}

bool CCouncilofRicksManager::CouncilorRateCheck(const CCouncilofRicksObject& govobj, bool fUpdateFailStatus)
{
    bool fRateCheckBypassed;
    return CouncilorRateCheck(govobj, fUpdateFailStatus, true, fRateCheckBypassed);
}

bool CCouncilofRicksManager::CouncilorRateCheck(const CCouncilofRicksObject& govobj, bool fUpdateFailStatus, bool fForce, bool& fRateCheckBypassed)
{
    LOCK(cs);

    fRateCheckBypassed = false;

    if(!councilorSync.IsSynced()) {
        return true;
    }

    if(!fRateChecksEnabled) {
        return true;
    }

    int nObjectType = govobj.GetObjectType();
    if((nObjectType != COUNCILOFRICKS_OBJECT_TRIGGER) && (nObjectType != COUNCILOFRICKS_OBJECT_WATCHDOG)) {
        return true;
    }

    const CTxIn& vin = govobj.GetCouncilorVin();
    int64_t nTimestamp = govobj.GetCreationTime();
    int64_t nNow = GetAdjustedTime();
    int64_t nSuperblockCycleSeconds = Params().GetConsensus().nSuperblockCycle * Params().GetConsensus().nPowTargetSpacing;

    std::string strHash = govobj.GetHash().ToString();

    if(nTimestamp < nNow - 2 * nSuperblockCycleSeconds) {
        LogPrintf("CCouncilofRicksManager::CouncilorRateCheck -- object %s rejected due to too old timestamp, councilor vin = %s, timestamp = %d, current time = %d\n",
                 strHash, vin.prevout.ToStringShort(), nTimestamp, nNow);
        return false;
    }

    if(nTimestamp > nNow + MAX_TIME_FUTURE_DEVIATION) {
        LogPrintf("CCouncilofRicksManager::CouncilorRateCheck -- object %s rejected due to too new (future) timestamp, councilor vin = %s, timestamp = %d, current time = %d\n",
                 strHash, vin.prevout.ToStringShort(), nTimestamp, nNow);
        return false;
    }

    txout_m_it it  = mapLastCouncilorObject.find(vin.prevout);
    if(it == mapLastCouncilorObject.end())
        return true;

    if(it->second.fStatusOK && !fForce) {
        fRateCheckBypassed = true;
        return true;
    }

    double dMaxRate = 1.1 / nSuperblockCycleSeconds;
    double dRate = 0.0;
    CRateCheckBuffer buffer;
    switch(nObjectType) {
    case COUNCILOFRICKS_OBJECT_TRIGGER:
        // Allow 1 trigger per mn per cycle, with a small fudge factor
        buffer = it->second.triggerBuffer;
        dMaxRate = 2 * 1.1 / double(nSuperblockCycleSeconds);
        break;
    case COUNCILOFRICKS_OBJECT_WATCHDOG:
        buffer = it->second.watchdogBuffer;
        dMaxRate = 2 * 1.1 / 3600.;
        break;
    default:
        break;
    }

    buffer.AddTimestamp(nTimestamp);
    dRate = buffer.GetRate();

    bool fRateOK = ( dRate < dMaxRate );

    if(!fRateOK)
    {
        LogPrintf("CCouncilofRicksManager::CouncilorRateCheck -- Rate too high: object hash = %s, councilor vin = %s, object timestamp = %d, rate = %f, max rate = %f\n",
                  strHash, vin.prevout.ToStringShort(), nTimestamp, dRate, dMaxRate);

        if (fUpdateFailStatus)
            it->second.fStatusOK = false;
    }

    return fRateOK;
}

bool CCouncilofRicksManager::ProcessVote(CNode* pfrom, const CCouncilofRicksVote& vote, CCouncilofRicksException& exception, CConnman& connman)
{
    ENTER_CRITICAL_SECTION(cs);
    uint256 nHashVote = vote.GetHash();
    if(mapInvalidVotes.HasKey(nHashVote)) {
        std::ostringstream ostr;
        ostr << "CCouncilofRicksManager::ProcessVote -- Old invalid vote "
                << ", MN outpoint = " << vote.GetCouncilorOutpoint().ToStringShort()
                << ", councilofricks object hash = " << vote.GetParentHash().ToString();
        LogPrintf("%s\n", ostr.str());
        exception = CCouncilofRicksException(ostr.str(), COUNCILOFRICKS_EXCEPTION_PERMANENT_ERROR, 20);
        LEAVE_CRITICAL_SECTION(cs);
        return false;
    }

    uint256 nHashGovobj = vote.GetParentHash();
    object_m_it it = mapObjects.find(nHashGovobj);
    if(it == mapObjects.end()) {
        std::ostringstream ostr;
        ostr << "CCouncilofRicksManager::ProcessVote -- Unknown parent object "
             << ", MN outpoint = " << vote.GetCouncilorOutpoint().ToStringShort()
             << ", councilofricks object hash = " << vote.GetParentHash().ToString();
        exception = CCouncilofRicksException(ostr.str(), COUNCILOFRICKS_EXCEPTION_WARNING);
        if(mapOrphanVotes.Insert(nHashGovobj, vote_time_pair_t(vote, GetAdjustedTime() + COUNCILOFRICKS_ORPHAN_EXPIRATION_TIME))) {
            LEAVE_CRITICAL_SECTION(cs);
            RequestCouncilofRicksObject(pfrom, nHashGovobj, connman);
            LogPrintf("%s\n", ostr.str());
            return false;
        }

        LogPrint("gobject", "%s\n", ostr.str());
        LEAVE_CRITICAL_SECTION(cs);
        return false;
    }

    CCouncilofRicksObject& govobj = it->second;

    if(govobj.IsSetCachedDelete() || govobj.IsSetExpired()) {
        LogPrint("gobject", "CCouncilofRicksObject::ProcessVote -- ignoring vote for expired or deleted object, hash = %s\n", nHashGovobj.ToString());
        LEAVE_CRITICAL_SECTION(cs);
        return false;
    }

    bool fOk = govobj.ProcessVote(pfrom, vote, exception, connman);
    if(fOk) {
        mapVoteToObject.Insert(nHashVote, &govobj);

        if(govobj.GetObjectType() == COUNCILOFRICKS_OBJECT_WATCHDOG) {
            mnodeman.UpdateWatchdogVoteTime(vote.GetCouncilorOutpoint());
            LogPrint("gobject", "CCouncilofRicksObject::ProcessVote -- COUNCILOFRICKS_OBJECT_WATCHDOG vote for %s\n", vote.GetParentHash().ToString());
        }
    }
    LEAVE_CRITICAL_SECTION(cs);
    return fOk;
}

void CCouncilofRicksManager::CheckCouncilorOrphanVotes(CConnman& connman)
{
    LOCK2(cs_main, cs);

    ScopedLockBool guard(cs, fRateChecksEnabled, false);

    for(object_m_it it = mapObjects.begin(); it != mapObjects.end(); ++it) {
        it->second.CheckOrphanVotes(connman);
    }
}

void CCouncilofRicksManager::CheckCouncilorOrphanObjects(CConnman& connman)
{
    LOCK2(cs_main, cs);
    int64_t nNow = GetAdjustedTime();
    ScopedLockBool guard(cs, fRateChecksEnabled, false);
    object_info_m_it it = mapCouncilorOrphanObjects.begin();
    while(it != mapCouncilorOrphanObjects.end()) {
        object_info_pair_t& pair = it->second;
        CCouncilofRicksObject& govobj = pair.first;

        if(pair.second.nExpirationTime >= nNow) {
            string strError;
            bool fCouncilorMissing = false;
            bool fConfirmationsMissing = false;
            bool fIsValid = govobj.IsValidLocally(strError, fCouncilorMissing, fConfirmationsMissing, true);

            if(fIsValid) {
                AddCouncilofRicksObject(govobj, connman);
            } else if(fCouncilorMissing) {
                ++it;
                continue;
            }
        } else {
            // apply node's ban score
            Misbehaving(pair.second.idFrom, 20);
        }

        auto it_count = mapCouncilorOrphanCounter.find(govobj.GetCouncilorVin().prevout);
        if(--it_count->second == 0)
            mapCouncilorOrphanCounter.erase(it_count);

        mapCouncilorOrphanObjects.erase(it++);
    }
}

void CCouncilofRicksManager::CheckPostponedObjects(CConnman& connman)
{
    if(!councilorSync.IsSynced()) return;

    LOCK2(cs_main, cs);

    // Check postponed proposals
    for(object_m_it it = mapPostponedObjects.begin(); it != mapPostponedObjects.end();) {

        const uint256& nHash = it->first;
        CCouncilofRicksObject& govobj = it->second;

        assert(govobj.GetObjectType() != COUNCILOFRICKS_OBJECT_WATCHDOG &&
               govobj.GetObjectType() != COUNCILOFRICKS_OBJECT_TRIGGER);

        std::string strError;
        bool fMissingConfirmations;
        if (govobj.IsCollateralValid(strError, fMissingConfirmations))
        {
            if(govobj.IsValidLocally(strError, false))
                AddCouncilofRicksObject(govobj, connman);
            else
                LogPrintf("CCouncilofRicksManager::CheckPostponedObjects -- %s invalid\n", nHash.ToString());

        } else if(fMissingConfirmations) {
            // wait for more confirmations
            ++it;
            continue;
        }

        // remove processed or invalid object from the queue
        mapPostponedObjects.erase(it++);
    }


    // Perform additional relays for triggers/watchdogs
    int64_t nNow = GetAdjustedTime();
    int64_t nSuperblockCycleSeconds = Params().GetConsensus().nSuperblockCycle * Params().GetConsensus().nPowTargetSpacing;

    for(hash_s_it it = setAdditionalRelayObjects.begin(); it != setAdditionalRelayObjects.end();) {

        object_m_it itObject = mapObjects.find(*it);
        if(itObject != mapObjects.end()) {

            CCouncilofRicksObject& govobj = itObject->second;

            int64_t nTimestamp = govobj.GetCreationTime();

            bool fValid = (nTimestamp <= nNow + MAX_TIME_FUTURE_DEVIATION) && (nTimestamp >= nNow - 2 * nSuperblockCycleSeconds);
            bool fReady = (nTimestamp <= nNow + MAX_TIME_FUTURE_DEVIATION - RELIABLE_PROPAGATION_TIME);

            if(fValid) {
                if(fReady) {
                    LogPrintf("CCouncilofRicksManager::CheckPostponedObjects -- additional relay: hash = %s\n", govobj.GetHash().ToString());
                    govobj.Relay(connman);
                } else {
                    it++;
                    continue;
                }
            }

        } else {
            LogPrintf("CCouncilofRicksManager::CheckPostponedObjects -- additional relay of unknown object: %s\n", it->ToString());
        }

        setAdditionalRelayObjects.erase(it++);
    }
}

void CCouncilofRicksManager::RequestCouncilofRicksObject(CNode* pfrom, const uint256& nHash, CConnman& connman, bool fUseFilter)
{
    if(!pfrom) {
        return;
    }

    LogPrint("gobject", "CCouncilofRicksObject::RequestCouncilofRicksObject -- hash = %s (peer=%d)\n", nHash.ToString(), pfrom->GetId());

    if(pfrom->nVersion < COUNCILOFRICKS_FILTER_PROTO_VERSION) {
        connman.PushMessage(pfrom, NetMsgType::MNCOUNCILOFRICKSSYNC, nHash);
        return;
    }

    CBloomFilter filter;
    filter.clear();

    int nVoteCount = 0;
    if(fUseFilter) {
        LOCK(cs);
        CCouncilofRicksObject* pObj = FindCouncilofRicksObject(nHash);

        if(pObj) {
            filter = CBloomFilter(Params().GetConsensus().nCouncilofRicksFilterElements, COUNCILOFRICKS_FILTER_FP_RATE, GetRandInt(999999), BLOOM_UPDATE_ALL);
            std::vector<CCouncilofRicksVote> vecVotes = pObj->GetVoteFile().GetVotes();
            nVoteCount = vecVotes.size();
            for(size_t i = 0; i < vecVotes.size(); ++i) {
                filter.insert(vecVotes[i].GetHash());
            }
        }
    }

    LogPrint("gobject", "CCouncilofRicksManager::RequestCouncilofRicksObject -- nHash %s nVoteCount %d peer=%d\n", nHash.ToString(), nVoteCount, pfrom->id);
    connman.PushMessage(pfrom, NetMsgType::MNCOUNCILOFRICKSSYNC, nHash, filter);
}

int CCouncilofRicksManager::RequestCouncilofRicksObjectVotes(CNode* pnode, CConnman& connman)
{
    if(pnode->nVersion < MIN_COUNCILOFRICKS_PEER_PROTO_VERSION) return -3;
    std::vector<CNode*> vNodesCopy;
    vNodesCopy.push_back(pnode);
    return RequestCouncilofRicksObjectVotes(vNodesCopy, connman);
}

int CCouncilofRicksManager::RequestCouncilofRicksObjectVotes(const std::vector<CNode*>& vNodesCopy, CConnman& connman)
{
    static std::map<uint256, std::map<CService, int64_t> > mapAskedRecently;

    if(vNodesCopy.empty()) return -1;

    int64_t nNow = GetTime();
    int nTimeout = 60 * 60;
    size_t nPeersPerHashMax = 3;

    std::vector<CCouncilofRicksObject*> vpGovObjsTmp;
    std::vector<CCouncilofRicksObject*> vpGovObjsTriggersTmp;

    // This should help us to get some idea about an impact this can bring once deployed on mainnet.
    // Testnet is ~40 times smaller in councilor count, but only ~1000 councilors usually vote,
    // so 1 obj on mainnet == ~10 objs or ~1000 votes on testnet. However we want to test a higher
    // number of votes to make sure it's robust enough, so aim at 2000 votes per councilor per request.
    // On mainnet nMaxObjRequestsPerNode is always set to 1.
    int nMaxObjRequestsPerNode = 1;
    size_t nProjectedVotes = 2000;
    if(Params().NetworkIDString() != CBaseChainParams::MAIN) {
        nMaxObjRequestsPerNode = std::max(1, int(nProjectedVotes / std::max(1, mnodeman.size())));
    }

    {
        LOCK2(cs_main, cs);

        if(mapObjects.empty()) return -2;

        for(object_m_it it = mapObjects.begin(); it != mapObjects.end(); ++it) {
            if(mapAskedRecently.count(it->first)) {
                std::map<CService, int64_t>::iterator it1 = mapAskedRecently[it->first].begin();
                while(it1 != mapAskedRecently[it->first].end()) {
                    if(it1->second < nNow) {
                        mapAskedRecently[it->first].erase(it1++);
                    } else {
                        ++it1;
                    }
                }
                if(mapAskedRecently[it->first].size() >= nPeersPerHashMax) continue;
            }
            if(it->second.nObjectType == COUNCILOFRICKS_OBJECT_TRIGGER) {
                vpGovObjsTriggersTmp.push_back(&(it->second));
            } else {
                vpGovObjsTmp.push_back(&(it->second));
            }
        }
    }

    LogPrint("gobject", "CCouncilofRicksManager::RequestCouncilofRicksObjectVotes -- start: vpGovObjsTriggersTmp %d vpGovObjsTmp %d mapAskedRecently %d\n",
                vpGovObjsTriggersTmp.size(), vpGovObjsTmp.size(), mapAskedRecently.size());

    InsecureRand insecureRand;
    // shuffle pointers
    std::random_shuffle(vpGovObjsTriggersTmp.begin(), vpGovObjsTriggersTmp.end(), insecureRand);
    std::random_shuffle(vpGovObjsTmp.begin(), vpGovObjsTmp.end(), insecureRand);

    for (int i = 0; i < nMaxObjRequestsPerNode; ++i) {
        uint256 nHashGovobj;

        // ask for triggers first
        if(vpGovObjsTriggersTmp.size()) {
            nHashGovobj = vpGovObjsTriggersTmp.back()->GetHash();
        } else {
            if(vpGovObjsTmp.empty()) break;
            nHashGovobj = vpGovObjsTmp.back()->GetHash();
        }
        bool fAsked = false;
        BOOST_FOREACH(CNode* pnode, vNodesCopy) {
            // Only use regular peers, don't try to ask from outbound "councilor" connections -
            // they stay connected for a short period of time and it's possible that we won't get everything we should.
            // Only use outbound connections - inbound connection could be a "councilor" connection
            // initiated from another node, so skip it too.
            if(pnode->fCouncilor || (fCounciLor && pnode->fInbound)) continue;
            // only use up to date peers
            if(pnode->nVersion < MIN_COUNCILOFRICKS_PEER_PROTO_VERSION) continue;
            // stop early to prevent setAskFor overflow
            size_t nProjectedSize = pnode->setAskFor.size() + nProjectedVotes;
            if(nProjectedSize > SETASKFOR_MAX_SZ/2) continue;
            // to early to ask the same node
            if(mapAskedRecently[nHashGovobj].count(pnode->addr)) continue;

            RequestCouncilofRicksObject(pnode, nHashGovobj, connman, true);
            mapAskedRecently[nHashGovobj][pnode->addr] = nNow + nTimeout;
            fAsked = true;
            // stop loop if max number of peers per obj was asked
            if(mapAskedRecently[nHashGovobj].size() >= nPeersPerHashMax) break;
        }
        // NOTE: this should match `if` above (the one before `while`)
        if(vpGovObjsTriggersTmp.size()) {
            vpGovObjsTriggersTmp.pop_back();
        } else {
            vpGovObjsTmp.pop_back();
        }
        if(!fAsked) i--;
    }
    LogPrint("gobject", "CCouncilofRicksManager::RequestCouncilofRicksObjectVotes -- end: vpGovObjsTriggersTmp %d vpGovObjsTmp %d mapAskedRecently %d\n",
                vpGovObjsTriggersTmp.size(), vpGovObjsTmp.size(), mapAskedRecently.size());

    return int(vpGovObjsTriggersTmp.size() + vpGovObjsTmp.size());
}

bool CCouncilofRicksManager::AcceptObjectMessage(const uint256& nHash)
{
    LOCK(cs);
    return AcceptMessage(nHash, setRequestedObjects);
}

bool CCouncilofRicksManager::AcceptVoteMessage(const uint256& nHash)
{
    LOCK(cs);
    return AcceptMessage(nHash, setRequestedVotes);
}

bool CCouncilofRicksManager::AcceptMessage(const uint256& nHash, hash_s_t& setHash)
{
    hash_s_it it = setHash.find(nHash);
    if(it == setHash.end()) {
        // We never requested this
        return false;
    }
    // Only accept one response
    setHash.erase(it);
    return true;
}

void CCouncilofRicksManager::RebuildIndexes()
{
    mapVoteToObject.Clear();
    for(object_m_it it = mapObjects.begin(); it != mapObjects.end(); ++it) {
        CCouncilofRicksObject& govobj = it->second;
        std::vector<CCouncilofRicksVote> vecVotes = govobj.GetVoteFile().GetVotes();
        for(size_t i = 0; i < vecVotes.size(); ++i) {
            mapVoteToObject.Insert(vecVotes[i].GetHash(), &govobj);
        }
    }
}

void CCouncilofRicksManager::AddCachedTriggers()
{
    LOCK(cs);

    for(object_m_it it = mapObjects.begin(); it != mapObjects.end(); ++it) {
        CCouncilofRicksObject& govobj = it->second;

        if(govobj.nObjectType != COUNCILOFRICKS_OBJECT_TRIGGER) {
            continue;
        }

        triggerman.AddNewTrigger(govobj.GetHash());
    }
}

void CCouncilofRicksManager::InitOnLoad()
{
    LOCK(cs);
    int64_t nStart = GetTimeMillis();
    LogPrintf("Preparing councilor indexes and councilofricks triggers...\n");
    RebuildIndexes();
    AddCachedTriggers();
    LogPrintf("Councilor indexes and councilofricks triggers prepared  %dms\n", GetTimeMillis() - nStart);
    LogPrintf("     %s\n", ToString());
}

std::string CCouncilofRicksManager::ToString() const
{
    LOCK(cs);

    int nProposalCount = 0;
    int nTriggerCount = 0;
    int nWatchdogCount = 0;
    int nOtherCount = 0;

    object_m_cit it = mapObjects.begin();

    while(it != mapObjects.end()) {
        switch(it->second.GetObjectType()) {
            case COUNCILOFRICKS_OBJECT_PROPOSAL:
                nProposalCount++;
                break;
            case COUNCILOFRICKS_OBJECT_TRIGGER:
                nTriggerCount++;
                break;
            case COUNCILOFRICKS_OBJECT_WATCHDOG:
                nWatchdogCount++;
                break;
            default:
                nOtherCount++;
                break;
        }
        ++it;
    }

    return strprintf("CouncilofRicks Objects: %d (Proposals: %d, Triggers: %d, Watchdogs: %d/%d, Other: %d; Erased: %d), Votes: %d",
                    (int)mapObjects.size(),
                    nProposalCount, nTriggerCount, nWatchdogCount, mapWatchdogObjects.size(), nOtherCount, (int)mapErasedCouncilofRicksObjects.size(),
                    (int)mapVoteToObject.GetSize());
}

void CCouncilofRicksManager::UpdatedBlockTip(const CBlockIndex *pindex, CConnman& connman)
{
    // Note this gets called from ActivateBestChain without cs_main being held
    // so it should be safe to lock our mutex here without risking a deadlock
    // On the other hand it should be safe for us to access pindex without holding a lock
    // on cs_main because the CBlockIndex objects are dynamically allocated and
    // presumably never deleted.
    if(!pindex) {
        return;
    }

    nCachedBlockHeight = pindex->nHeight;
    LogPrint("gobject", "CCouncilofRicksManager::UpdatedBlockTip -- nCachedBlockHeight: %d\n", nCachedBlockHeight);

    CheckPostponedObjects(connman);
}

void CCouncilofRicksManager::RequestOrphanObjects(CConnman& connman)
{
    std::vector<CNode*> vNodesCopy = connman.CopyNodeVector();

    std::vector<uint256> vecHashesFiltered;
    {
        std::vector<uint256> vecHashes;
        LOCK(cs);
        mapOrphanVotes.GetKeys(vecHashes);
        for(size_t i = 0; i < vecHashes.size(); ++i) {
            const uint256& nHash = vecHashes[i];
            if(mapObjects.find(nHash) == mapObjects.end()) {
                vecHashesFiltered.push_back(nHash);
            }
        }
    }

    LogPrint("gobject", "CCouncilofRicksObject::RequestOrphanObjects -- number objects = %d\n", vecHashesFiltered.size());
    for(size_t i = 0; i < vecHashesFiltered.size(); ++i) {
        const uint256& nHash = vecHashesFiltered[i];
        for(size_t j = 0; j < vNodesCopy.size(); ++j) {
            CNode* pnode = vNodesCopy[j];
            if(pnode->fCouncilor) {
                continue;
            }
            RequestCouncilofRicksObject(pnode, nHash, connman);
        }
    }

    connman.ReleaseNodeVector(vNodesCopy);
}

void CCouncilofRicksManager::CleanOrphanObjects()
{
    LOCK(cs);
    const vote_mcache_t::list_t& items = mapOrphanVotes.GetItemList();

    int64_t nNow = GetAdjustedTime();

    vote_mcache_t::list_cit it = items.begin();
    while(it != items.end()) {
        vote_mcache_t::list_cit prevIt = it;
        ++it;
        const vote_time_pair_t& pairVote = prevIt->value;
        if(pairVote.second < nNow) {
            mapOrphanVotes.Erase(prevIt->key, prevIt->value);
        }
    }
}
