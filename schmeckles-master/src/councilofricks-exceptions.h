// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef COUNCILOFRICKS_EXCEPTIONS_H
#define COUNCILOFRICKS_EXCEPTIONS_H

#include <exception>
#include <iostream>
#include <sstream>
#include <string>

enum councilofricks_exception_type_enum_t {
    /// Default value, normally indicates no exception condition occurred
    COUNCILOFRICKS_EXCEPTION_NONE = 0,
    /// Unusual condition requiring no caller action
    COUNCILOFRICKS_EXCEPTION_WARNING = 1,
    /// Requested operation cannot be performed
    COUNCILOFRICKS_EXCEPTION_PERMANENT_ERROR = 2,
    /// Requested operation not currently possible, may resubmit later
    COUNCILOFRICKS_EXCEPTION_TEMPORARY_ERROR = 3,
    /// Unexpected error (ie. should not happen unless there is a bug in the code)
    COUNCILOFRICKS_EXCEPTION_INTERNAL_ERROR = 4
};

inline std::ostream& operator<<(std::ostream& os, councilofricks_exception_type_enum_t eType)
{
    switch(eType) {
    case COUNCILOFRICKS_EXCEPTION_NONE:
        os << "COUNCILOFRICKS_EXCEPTION_NONE";
        break;
    case COUNCILOFRICKS_EXCEPTION_WARNING:
        os << "COUNCILOFRICKS_EXCEPTION_WARNING";
        break;
    case COUNCILOFRICKS_EXCEPTION_PERMANENT_ERROR:
        os << "COUNCILOFRICKS_EXCEPTION_PERMANENT_ERROR";
        break;
    case COUNCILOFRICKS_EXCEPTION_TEMPORARY_ERROR:
        os << "COUNCILOFRICKS_EXCEPTION_TEMPORARY_ERROR";
        break;
    case COUNCILOFRICKS_EXCEPTION_INTERNAL_ERROR:
        os << "COUNCILOFRICKS_EXCEPTION_INTERNAL_ERROR";
        break;
    }
    return os;
}

/**
 * A class which encapsulates information about a councilofricks exception condition
 *
 * Derives from std::exception so is suitable for throwing
 * (ie. will be caught by a std::exception handler) but may also be used as a
 * normal object.
 */
class CCouncilofRicksException : public std::exception
{
private:
    std::string strMessage;

    councilofricks_exception_type_enum_t eType;

    int nNodePenalty;

public:
    CCouncilofRicksException(const std::string& strMessageIn = "",
                         councilofricks_exception_type_enum_t eTypeIn = COUNCILOFRICKS_EXCEPTION_NONE,
                         int nNodePenaltyIn = 0)
        : strMessage(),
          eType(eTypeIn),
          nNodePenalty(nNodePenaltyIn)
    {
        std::ostringstream ostr;
        ostr << eType << ":" << strMessageIn;
        strMessage = ostr.str();
    }

    virtual ~CCouncilofRicksException() throw() {}

    virtual const char* what() const throw()
    {
        return strMessage.c_str();
    }

    const std::string& GetMessage() const
    {
        return strMessage;
    }

    councilofricks_exception_type_enum_t GetType() const
    {
        return eType;
    }

    int GetNodePenalty() const {
        return nNodePenalty;
    }
};

#endif
