
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2012 The Bitcoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#ifndef ACTIVECOUNCILOR_H
#define ACTIVECOUNCILOR_H

#include "bignum.h"
#include "sync.h"
#include "net.h"
#include "key.h"
#include "core.h"
#include "init.h"
#include "wallet.h"

class CActiveCouncilor;
extern CActiveCouncilor activeCouncilor;

// Responsible for activating the councilor and pinging the network
class CActiveCouncilor
{
public:
    CTxIn vinCouncilor;
    CPubKey pubkeyCounciLor;
    CPubKey pubkeyCounciLor2;

    std::string strCounciLorSignMessage;
    std::vector<unsigned char> vchCounciLorSignature;

    std::string masterNodeAddr;
    CService masterNodeSignAddr;

    int isCapableCounciLor;
    int64 masterNodeSignatureTime;
    int councilorPortOpen;

    CActiveCouncilor()
    {        
        isCapableCounciLor = COUNCILOR_NOT_PROCESSED;
        councilorPortOpen = 0;
    }

    // get 1000SMK input that can be used for the councilor
    bool GetCounciLorVin(CTxIn& vin, CPubKey& pubkey, CKey& secretKey);

    // start the councilor and register with the network
    void RegisterAsCounciLor(bool stop);
    // start a remote councilor
    bool RegisterAsCounciLorRemoteOnly(std::string strCounciLorAddr, std::string strCounciLorPrivKey);

    // enable hot wallet mode (run a councilor with no funds)
    bool EnableHotColdCounciLor(CTxIn& vin, int64 sigTime, CService& addr);
};

#endif