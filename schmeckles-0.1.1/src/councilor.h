
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2012 The Bitcoin developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#ifndef COUNCILOR_H
#define COUNCILOR_H

#include "bignum.h"
#include "sync.h"
#include "net.h"
#include "key.h"
#include "core.h"
#include "util.h"
#include "script.h"
#include "hashblock.h"
#include "base58.h"
#include "main.h"

class CCounciLor;
class CCouncilorPayments;

#define COUNCILOR_NOT_PROCESSED               0 // initial state
#define COUNCILOR_IS_CAPABLE                  1
#define COUNCILOR_NOT_CAPABLE                 2
#define COUNCILOR_STOPPED                     3
#define COUNCILOR_INPUT_TOO_NEW               4
#define COUNCILOR_PORT_NOT_OPEN               6 
#define COUNCILOR_PORT_OPEN                   7
#define COUNCILOR_SYNC_IN_PROCESS             8
#define COUNCILOR_REMOTELY_ENABLED            9

#define COUNCILOR_MIN_CONFIRMATIONS           15
#define COUNCILOR_MIN_DSEEP_SECONDS           (30*60)
#define COUNCILOR_MIN_DSEE_SECONDS            (5*60)
#define COUNCILOR_PING_SECONDS                (1*60)
#define COUNCILOR_EXPIRATION_SECONDS          (65*60)
#define COUNCILOR_REMOVAL_SECONDS             (70*60)

using namespace std;

extern std::vector<CCounciLor> darkSendCounciLors;
extern CCouncilorPayments councilorPayments;
extern std::vector<CTxIn> vecCouncilorAskedFor;
extern map<uint256, int> mapSeenCouncilorVotes;


// manage the councilor connections
void ProcessCouncilorConnections();
int CountCouncilorsAboveProtocol(int protocolVersion);

// Get the current winner for this block
int GetCurrentCounciLor(int mod=1, int64 nBlockHeight=0, int minProtocol=0);

int GetCouncilorByVin(CTxIn& vin);
int GetCouncilorRank(CTxIn& vin, int64 nBlockHeight=0, int minProtocol=0);
int GetCouncilorByRank(int findRank, int64 nBlockHeight=0, int minProtocol=0);

void ProcessMessageCouncilor(CNode* pfrom, std::string& strCommand, CDataStream& vRecv);

// 
// The Councilor Class. For managing the darksend process. It contains the input of the 1000SMK, signature to prove
// it's the one who own that ip address and code for calculating the payment election.
//  
class CCounciLor
{
public:
    CService addr;
    CTxIn vin;
    int64 lastTimeSeen;
    CPubKey pubkey;
    CPubKey pubkey2;
    std::vector<unsigned char> sig;
    int64 now; //dsee message times
    int64 lastDseep;
    int cacheInputAge;
    int cacheInputAgeBlock;
    int enabled;
    bool unitTest;
    bool allowFreeTx;
    int protocolVersion;

    //the dsq count from the last dsq broadcast of this node
    int64 nLastDsq;

    CCounciLor(CService newAddr, CTxIn newVin, CPubKey newPubkey, std::vector<unsigned char> newSig, int64 newNow, CPubKey newPubkey2, int protocolVersionIn)
    {
        addr = newAddr;
        vin = newVin;
        pubkey = newPubkey;
        pubkey2 = newPubkey2;
        sig = newSig;
        now = newNow;
        enabled = 1;
        lastTimeSeen = 0;
        unitTest = false;    
        cacheInputAge = 0;
        cacheInputAgeBlock = 0;
        nLastDsq = 0;
        lastDseep = 0;
        allowFreeTx = true;
        protocolVersion = protocolVersionIn;
    }

    uint256 CalculateScore(int mod=1, int64 nBlockHeight=0);

    void UpdateLastSeen(int64 override=0)
    {
        if(override == 0){
            lastTimeSeen = GetAdjustedTime();
        } else {
            lastTimeSeen = override;
        }
    }

    inline uint64 SliceHash(uint256& hash, int slice)
    {
        uint64 n = 0;
        memcpy(&n, &hash+slice*64, 64);
        return n;
    }

    void Check();

    bool UpdatedWithin(int seconds)
    {
        //LogPrintf("UpdatedWithin %"PRI64u", %"PRI64u" --  %d \n", GetTimeMicros() , lastTimeSeen, (GetTimeMicros() - lastTimeSeen) < seconds);

        return (GetAdjustedTime() - lastTimeSeen) < seconds;
    }

    void Disable()
    {
        lastTimeSeen = 0;
    }

    bool IsEnabled()
    {
        return enabled == 1;
    }

    int GetCouncilorInputAge()
    {
        if(pindexBest == NULL) return 0;

        if(cacheInputAge == 0){
            cacheInputAge = GetInputAge(vin);
            cacheInputAgeBlock = pindexBest->nHeight;
        }

        return cacheInputAge+(pindexBest->nHeight-cacheInputAgeBlock);
    }
};


// for storing the winning payments
class CCouncilorPaymentWinner
{
public:
    int nBlockHeight;
    CTxIn vin;
    std::vector<unsigned char> vchSig;
    uint64 score;

    CCouncilorPaymentWinner() {
        nBlockHeight = 0;
        score = 0;
        vin = CTxIn();
    }

    uint256 GetHash(){
        uint256 n2 = HashX11(BEGIN(nBlockHeight), END(nBlockHeight));
        uint256 n3 = vin.prevout.hash > n2 ? (vin.prevout.hash - n2) : (n2 - vin.prevout.hash);

        return n3;
    }

    IMPLEMENT_SERIALIZE(
        READWRITE(nBlockHeight);
        READWRITE(score);
        READWRITE(vin);
        READWRITE(vchSig);
     )
};

//
// Councilor Payments Class 
// Keeps track of who should get paid for which blocks
//

class CCouncilorPayments
{
private:
    std::vector<CCouncilorPaymentWinner> vWinning;
    int nSyncedFromPeer;
    std::string strMasterPrivKey;
    std::string strTestPubKey;
    std::string strMainPubKey;

public:

    CCouncilorPayments() {
        strMainPubKey = "04549ac134f694c0243f503e8c8a9a986f5de6610049c40b07816809b0d1d06a21b07be27b9bb555931773f62ba6cf35a25fd52f694d4e1106ccd237a7bb899fdd";
        strTestPubKey = "046f78dcf911fbd61910136f7f0f8d90578f68d0b3ac973b5040fb7afb501b5939f39b108b0569dca71488f5bbf498d92e4d1194f6f941307ffd95f75e76869f0e";
    }

    bool SetPrivKey(std::string strPrivKey);
    bool CheckSignature(CCouncilorPaymentWinner& winner);
    bool Sign(CCouncilorPaymentWinner& winner);
    
    // Deterministically calculate a given "score" for a councilor depending on how close it's hash is 
    // to the blockHeight. The further away they are the better, the furthest will win the election 
    // and get paid this block
    // 

    uint64 CalculateScore(uint256 blockHash, CTxIn& vin);
    bool GetWinningCouncilor(int nBlockHeight, CTxIn& vinOut);
    bool AddWinningCouncilor(CCouncilorPaymentWinner& winner);
    bool ProcessBlock(int nBlockHeight);
    void Relay(CCouncilorPaymentWinner& winner);
    void Sync(CNode* node);
    void CleanPaymentList();
    int LastPayment(CCounciLor& mn);

    //slow
    bool GetBlockPayee(int nBlockHeight, CScript& payee);
};

/*//
// Councilor Scanning Error 
// Enforces proof-of-service by scanning the councilor network
//

class CCouncilorPayments
{
private:
    std::vector<CCouncilorPaymentWinner> vWinning;
    int nSyncedFromPeer;
    std::string strMasterPrivKey;
    std::string strTestPubKey;
    std::string strMainPubKey;

public:

    CCouncilorPayments() {
        strMainPubKey = "04549ac134f694c0243f503e8c8a9a986f5de6610049c40b07816809b0d1d06a21b07be27b9bb555931773f62ba6cf35a25fd52f694d4e1106ccd237a7bb899fdd";
        strTestPubKey = "046f78dcf911fbd61910136f7f0f8d90578f68d0b3ac973b5040fb7afb501b5939f39b108b0569dca71488f5bbf498d92e4d1194f6f941307ffd95f75e76869f0e";
    }

    bool SetPrivKey(std::string strPrivKey);
    bool CheckSignature(CCouncilorPaymentWinner& winner);
    bool Sign(CCouncilorPaymentWinner& winner);
    
    // Deterministically calculate a given "score" for a councilor depending on how close it's hash is 
    // to the blockHeight. The further away they are the better, the furthest will win the election 
    // and get paid this block
    // 

    uint64 CalculateScore(uint256 blockHash, CTxIn& vin);
    bool GetWinningCouncilor(int nBlockHeight, CTxIn& vinOut);
    bool AddWinningCouncilor(CCouncilorPaymentWinner& winner);
    bool ProcessBlock(int nBlockHeight);
    void Relay(CCouncilorPaymentWinner& winner);
    void Sync(CNode* node);
    void CleanPaymentList();
    int LastPayment(CCounciLor& mn);

    //slow
    bool GetBlockPayee(int nBlockHeight, CScript& payee);
};*/


#endif
