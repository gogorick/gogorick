
#include "core.h"
#include "activecouncilor.h"

using namespace std;
using namespace boost;

CActiveCouncilor activeCouncilor;

//
// Bootup the councilor, look for a 10000SMK input and register on the network
//
void CActiveCouncilor::RegisterAsCounciLor(bool stop)
{
    if(!fCounciLor) return;

    //need correct adjusted time to send ping
    bool fIsInitialDownload = IsInitialBlockDownload();
    if(fIsInitialDownload) {
        isCapableCounciLor = COUNCILOR_SYNC_IN_PROCESS;
        LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Sync in progress. Must wait until sync is complete to start councilor.\n");
        return;
    }

    std::string errorMessage;

    CKey key2;
    CPubKey pubkey2;

    if(!darkSendSigner.SetKey(strCounciLorPrivKey, errorMessage, key2, pubkey2))
    {
        LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Invalid councilorprivkey: '%s'\n", errorMessage.c_str());
        exit(0);
    }

    if(isCapableCounciLor == COUNCILOR_INPUT_TOO_NEW || isCapableCounciLor == COUNCILOR_NOT_CAPABLE || isCapableCounciLor == COUNCILOR_SYNC_IN_PROCESS){
        isCapableCounciLor = COUNCILOR_NOT_PROCESSED;
    }

    if(isCapableCounciLor == COUNCILOR_NOT_PROCESSED) {
        if(strCounciLorAddr.empty()) {
            if(!GetLocal(masterNodeSignAddr)) {
                LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Can't detect external address. Please use the counciloraddr configuration option.\n");
                isCapableCounciLor = COUNCILOR_NOT_CAPABLE;
                return;
            }
        } else {
            masterNodeSignAddr = CService(strCounciLorAddr);
        }

        if((fTestNet && masterNodeSignAddr.GetPort() != 19999) || (!fTestNet && masterNodeSignAddr.GetPort() != 9999)) {
            LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Invalid port\n");
            isCapableCounciLor = COUNCILOR_NOT_CAPABLE;
            return;
        }

        LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Checking inbound connection to '%s'\n", masterNodeSignAddr.ToString().c_str());

        if(ConnectNode((CAddress)masterNodeSignAddr, masterNodeSignAddr.ToString().c_str())){
            councilorPortOpen = COUNCILOR_PORT_OPEN;
        } else {
            councilorPortOpen = COUNCILOR_PORT_NOT_OPEN;
            isCapableCounciLor = COUNCILOR_NOT_CAPABLE;
            LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Port not open.\n");
            return;
        }

        if(pwalletMain->IsLocked()){
            isCapableCounciLor = COUNCILOR_NOT_CAPABLE;
            LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Not capable.\n");
            return;
        }

        isCapableCounciLor = COUNCILOR_NOT_CAPABLE;

        CKey SecretKey;
        // Choose coins to use
        if(GetCounciLorVin(vinCouncilor, pubkeyCounciLor, SecretKey)) {

            if(GetInputAge(vinCouncilor) < COUNCILOR_MIN_CONFIRMATIONS){
                LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Input must have least %d confirmations - %d confirmations\n", COUNCILOR_MIN_CONFIRMATIONS, GetInputAge(vinCouncilor));
                isCapableCounciLor = COUNCILOR_INPUT_TOO_NEW;
                return;
            }

            int protocolVersion = PROTOCOL_VERSION;
            
            masterNodeSignatureTime = GetAdjustedTime();

            std::string vchPubKey(pubkeyCounciLor.begin(), pubkeyCounciLor.end());
            std::string vchPubKey2(pubkey2.begin(), pubkey2.end());
            std::string strMessage = masterNodeSignAddr.ToString() + boost::lexical_cast<std::string>(masterNodeSignatureTime) + vchPubKey + vchPubKey2 + boost::lexical_cast<std::string>(protocolVersion);

            if(!darkSendSigner.SignMessage(strMessage, errorMessage, vchCounciLorSignature, SecretKey)) {
                LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Sign message failed\n");
                return;
            }

            if(!darkSendSigner.VerifyMessage(pubkeyCounciLor, vchCounciLorSignature, strMessage, errorMessage)) {
                LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Verify message failed\n");
                return;
            }

            LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Is capable master node!\n");

            isCapableCounciLor = COUNCILOR_IS_CAPABLE;

            pwalletMain->LockCoin(vinCouncilor.prevout);

            bool found = false;
            BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors)
                if(mn.vin == vinCouncilor)
                    found = true;

            if(!found) {
                LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Adding myself to councilor list %s - %s\n", masterNodeSignAddr.ToString().c_str(), vinCouncilor.ToString().c_str());
                CCounciLor mn(masterNodeSignAddr, vinCouncilor, pubkeyCounciLor, vchCounciLorSignature, masterNodeSignatureTime, pubkey2, PROTOCOL_VERSION);
                mn.UpdateLastSeen(masterNodeSignatureTime);
                darkSendCounciLors.push_back(mn);
                LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Councilor input = %s\n", vinCouncilor.ToString().c_str());
            }

            //relay to all
            LOCK(cs_vNodes);
            BOOST_FOREACH(CNode* pnode, vNodes)
            {
                pnode->PushMessage("dsee", vinCouncilor, masterNodeSignAddr, vchCounciLorSignature, masterNodeSignatureTime, pubkeyCounciLor, pubkey2, -1, -1, masterNodeSignatureTime, protocolVersion);
            }

            return;
        }
    }

    if(isCapableCounciLor != COUNCILOR_IS_CAPABLE && isCapableCounciLor != COUNCILOR_REMOTELY_ENABLED)  return;

    masterNodeSignatureTime = GetAdjustedTime();

    std::string strMessage = masterNodeSignAddr.ToString() + boost::lexical_cast<std::string>(masterNodeSignatureTime) + boost::lexical_cast<std::string>(stop);

    if(!darkSendSigner.SignMessage(strMessage, errorMessage, vchCounciLorSignature, key2)) {
        LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Sign message failed\n");
        return;
    }

    if(!darkSendSigner.VerifyMessage(pubkey2, vchCounciLorSignature, strMessage, errorMessage)) {
        LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Verify message failed\n");
        return;
    }

    bool found = false;
    BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors) {
        //LogPrintf(" -- %s\n", mn.vin.ToString().c_str());

        if(mn.vin == vinCouncilor) {
            found = true;
            mn.UpdateLastSeen();
        }
    }
    if(!found){
        LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Darksend Councilor List doesn't include our councilor, Shutting down councilor pinging service! %s\n", vinCouncilor.ToString().c_str());
        isCapableCounciLor = COUNCILOR_STOPPED;
        return;
    }

    LogPrintf("CActiveCouncilor::RegisterAsCounciLor() - Councilor input = %s\n", vinCouncilor.ToString().c_str());

    if (stop) isCapableCounciLor = COUNCILOR_STOPPED;

    //relay to all peers
    LOCK(cs_vNodes);
    BOOST_FOREACH(CNode* pnode, vNodes)
    {
        pnode->PushMessage("dseep", vinCouncilor, vchCounciLorSignature, masterNodeSignatureTime, stop);
    }
}

//
// Bootup the councilor, look for a 10000SMK input and register on the network
// Takes 2 parameters to start a remote councilor
//
bool CActiveCouncilor::RegisterAsCounciLorRemoteOnly(std::string strCounciLorAddr, std::string strCounciLorPrivKey)
{
    if(!fCounciLor) return false;

    LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Address %s CounciLorPrivKey %s\n", strCounciLorAddr.c_str(), strCounciLorPrivKey.c_str());

    std::string errorMessage;

    CKey key2;
    CPubKey pubkey2;


    if(!darkSendSigner.SetKey(strCounciLorPrivKey, errorMessage, key2, pubkey2))
    {
        LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Invalid councilorprivkey: '%s'\n", errorMessage.c_str());
        return false;
    }

    CService masterNodeSignAddr = CService(strCounciLorAddr);
    BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors){
        if(mn.addr == masterNodeSignAddr){
            LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Address in use\n");
            return false;
        }
    }

    if((fTestNet && masterNodeSignAddr.GetPort() != 19999) || (!fTestNet && masterNodeSignAddr.GetPort() != 9999)) {
        LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Invalid port\n");
        return false;
    }

    LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Checking inbound connection to '%s'\n", masterNodeSignAddr.ToString().c_str());

    if(!ConnectNode((CAddress)masterNodeSignAddr, masterNodeSignAddr.ToString().c_str())){
        LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Error connecting to port\n");
        return false;
    }

    if(pwalletMain->IsLocked()){
        LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Wallet is locked\n");
        return false;
    }

    CKey SecretKey;
    CTxIn vinCouncilor;
    CPubKey pubkeyCounciLor;
    int masterNodeSignatureTime = 0;

    // Choose coins to use
    while (GetCounciLorVin(vinCouncilor, pubkeyCounciLor, SecretKey)) {
        // don't use a vin that's registered
        BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors)
            if(mn.vin == vinCouncilor)
                continue;

        if(GetInputAge(vinCouncilor) < COUNCILOR_MIN_CONFIRMATIONS)
            continue;

        masterNodeSignatureTime = GetAdjustedTime();

        std::string vchPubKey(pubkeyCounciLor.begin(), pubkeyCounciLor.end());
        std::string vchPubKey2(pubkey2.begin(), pubkey2.end());
        std::string strMessage = masterNodeSignAddr.ToString() + boost::lexical_cast<std::string>(masterNodeSignatureTime) + vchPubKey + vchPubKey2 + boost::lexical_cast<std::string>(PROTOCOL_VERSION);

        if(!darkSendSigner.SignMessage(strMessage, errorMessage, vchCounciLorSignature, SecretKey)) {
            LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Sign message failed\n");
            return false;
        }

        if(!darkSendSigner.VerifyMessage(pubkeyCounciLor, vchCounciLorSignature, strMessage, errorMessage)) {
            LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Verify message failed\n");
            return false;
        }

        LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - Is capable master node!\n");

        pwalletMain->LockCoin(vinCouncilor.prevout);

        int protocolVersion = PROTOCOL_VERSION;
        //relay to all
        LOCK(cs_vNodes);
        BOOST_FOREACH(CNode* pnode, vNodes)
        {
            pnode->PushMessage("dsee", vinCouncilor, masterNodeSignAddr, vchCounciLorSignature, masterNodeSignatureTime, pubkeyCounciLor, pubkey2, -1, -1, masterNodeSignatureTime, protocolVersion);
        }

        return true;
    }

    LogPrintf("CActiveCouncilor::RegisterAsCounciLorRemoteOnly() - No sutable vin found\n");
    return false;
}


bool CActiveCouncilor::GetCounciLorVin(CTxIn& vin, CPubKey& pubkey, CKey& secretKey)
{
    int64 nValueIn = 0;
    CScript pubScript;

    // try once before we try to denominate
    if (!pwalletMain->SelectCoinsCouncilor(vin, nValueIn, pubScript))
    {
        if(fDebug) LogPrintf("CActiveCouncilor::GetCounciLorVin - I'm not a capable councilor\n");
        return false;
    }

    CTxDestination address1;
    ExtractDestination(pubScript, address1);
    CBitcoinAddress address2(address1);

    CKeyID keyID;
    if (!address2.GetKeyID(keyID)) {
        LogPrintf("CActiveCouncilor::GetCounciLorVin - Address does not refer to a key\n");
        return false;
    }

    if (!pwalletMain->GetKey(keyID, secretKey)) {
        LogPrintf ("CActiveCouncilor::GetCounciLorVin - Private key for address is not known\n");
        return false;
    }

    pubkey = secretKey.GetPubKey();
    return true;
}

// when starting a councilor, this can enable to run as a hot wallet with no funds
bool CActiveCouncilor::EnableHotColdCounciLor(CTxIn& vin, int64 sigTime, CService& addr)
{
    if(!fCounciLor) return false;

    isCapableCounciLor = COUNCILOR_REMOTELY_ENABLED;

    vinCouncilor = vin;
    masterNodeSignatureTime = sigTime;
    masterNodeSignAddr = addr;

    LogPrintf("CActiveCouncilor::EnableHotColdCounciLor() - Enabled! You may shut down the cold daemon.\n");

    return true;
}
