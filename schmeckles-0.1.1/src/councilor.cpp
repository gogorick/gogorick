

#include "councilor.h"
#include "activecouncilor.h"
#include "darksend.h"
#include "core.h"


/** The list of active councilors */
std::vector<CCounciLor> darkSendCounciLors;
/** Object for who's going to get paid on which blocks */
CCouncilorPayments councilorPayments;
// keep track of councilor votes I've seen
map<uint256, int> mapSeenCouncilorVotes;
// keep track of the scanning errors I've seen
map<uint256, int> mapSeenCouncilorScanningErrors;
// who's asked for the councilor list and the last time
std::map<CNetAddr, int64> askedForCouncilorList;
// which councilors we've asked for
std::map<COutPoint, int64> askedForCouncilorListEntry;

// manage the councilor connections
void ProcessCouncilorConnections(){
    LOCK(cs_vNodes);

    BOOST_FOREACH(CNode* pnode, vNodes)
    {
        //if it's our councilor, let it be
        if(darkSendPool.submittedToCouncilor == pnode->addr) continue;

        if(pnode->fDarkSendMaster){
            LogPrintf("Closing councilor connection %s \n", pnode->addr.ToString().c_str());
            pnode->CloseSocketDisconnect();
        }
    }
}

void ProcessMessageCouncilor(CNode* pfrom, std::string& strCommand, CDataStream& vRecv)
{
    if (strCommand == "dsee") { //DarkSend Election Entry
        bool fIsInitialDownload = IsInitialBlockDownload();
        if(fIsInitialDownload) return;

        CTxIn vin; 
        CService addr;
        CPubKey pubkey;
        CPubKey pubkey2;
        vector<unsigned char> vchSig;
        int64 sigTime;
        int count;
        int current;
        int64 lastUpdated;
        int protocolVersion; 
        std::string strMessage;

        // 70047 and greater
        vRecv >> vin >> addr >> vchSig >> sigTime >> pubkey >> pubkey2 >> count >> current >> lastUpdated >> protocolVersion;

        // make sure signature isn't in the future (past is OK)
        if (sigTime > GetAdjustedTime() + 60 * 60) {
            LogPrintf("dsee - Signature rejected, too far into the future %s\n", vin.ToString().c_str());
            return;
        }

        bool isLocal = false; // addr.IsRFC1918();
        std::string vchPubKey(pubkey.begin(), pubkey.end());
        std::string vchPubKey2(pubkey2.begin(), pubkey2.end());

        strMessage = addr.ToString() + boost::lexical_cast<std::string>(sigTime) + vchPubKey + vchPubKey2 + boost::lexical_cast<std::string>(protocolVersion);
        
        if(protocolVersion < nCouncilorMinProtocol) {
            LogPrintf("dsee - ignoring outdated councilor %s protocol version %d\n", vin.ToString().c_str(), protocolVersion);
            return;
        }

        CScript pubkeyScript;
        pubkeyScript.SetDestination(pubkey.GetID());

        if(pubkeyScript.size() != 25) {
            LogPrintf("dsee - pubkey the wrong size\n");
            pfrom->Misbehaving(100);
            return;
        }

        CScript pubkeyScript2;
        pubkeyScript2.SetDestination(pubkey2.GetID());

        if(pubkeyScript2.size() != 25) {
            LogPrintf("dsee - pubkey the wrong size\n");
            pfrom->Misbehaving(100);
            return;
        }

        std::string errorMessage = "";
        if(!darkSendSigner.VerifyMessage(pubkey, vchSig, strMessage, errorMessage)){
            LogPrintf("dsee - Got bad councilor address signature\n");
            pfrom->Misbehaving(100);
            return;
        }

        if((fTestNet && addr.GetPort() != 19999) || (!fTestNet && addr.GetPort() != 9999)) return;

        //search existing councilor list, this is where we update existing councilors with new dsee broadcasts

        BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors) {
            if(mn.vin.prevout == vin.prevout) {
                // count == -1 when it's a new entry
                //   e.g. We don't want the entry relayed/time updated when we're syncing the list
                // mn.pubkey = pubkey, IsVinAssociatedWithPubkey is validated once below, 
                //   after that they just need to match
                if(count == -1 && mn.pubkey == pubkey && !mn.UpdatedWithin(COUNCILOR_MIN_DSEE_SECONDS)){
                    mn.UpdateLastSeen();

                    if(mn.now < sigTime){ //take the newest entry
                        LogPrintf("dsee - Got updated entry for %s\n", addr.ToString().c_str());
                        mn.pubkey2 = pubkey2;
                        mn.now = sigTime;
                        mn.sig = vchSig;
                        mn.protocolVersion = protocolVersion;
                        mn.addr = addr;

                        RelayDarkSendElectionEntry(vin, addr, vchSig, sigTime, pubkey, pubkey2, count, current, lastUpdated, protocolVersion);
                    }
                }

                return;
            }
        }

        // make sure the vout that was signed is related to the transaction that spawned the councilor
        //  - this is expensive, so it's only done once per councilor
        if(!darkSendSigner.IsVinAssociatedWithPubkey(vin, pubkey)) {
            LogPrintf("dsee - Got mismatched pubkey and vin\n");
            pfrom->Misbehaving(100);
            return;
        }

        if(fDebug) LogPrintf("dsee - Got NEW councilor entry %s\n", addr.ToString().c_str());

        // make sure it's still unspent
        //  - this is checked later by .check() in many places and by ThreadCheckDarkSendPool()

        CValidationState state;
        CTransaction tx = CTransaction();
        CTxOut vout = CTxOut(999.99*COIN, darkSendPool.collateralPubKey);
        tx.vin.push_back(vin);
        tx.vout.push_back(vout);
        if(tx.AcceptableInputs(state, true)){
            if(fDebug) LogPrintf("dsee - Accepted councilor entry %i %i\n", count, current);

            if(GetInputAge(vin) < COUNCILOR_MIN_CONFIRMATIONS){
                LogPrintf("dsee - Input must have least %d confirmations\n", COUNCILOR_MIN_CONFIRMATIONS);
                pfrom->Misbehaving(20);
                return;
            }

            // use this as a peer
            addrman.Add(CAddress(addr), pfrom->addr, 2*60*60);

            // add our councilor
            CCounciLor mn(addr, vin, pubkey, vchSig, sigTime, pubkey2, protocolVersion);
            mn.UpdateLastSeen(lastUpdated);
            darkSendCounciLors.push_back(mn);

            // if it matches our councilorprivkey, then we've been remotely activated
            if(pubkey2 == activeCouncilor.pubkeyCounciLor2 && protocolVersion == PROTOCOL_VERSION){
                activeCouncilor.EnableHotColdCounciLor(vin, sigTime, addr);
            }

            if(count == -1 && !isLocal)
                RelayDarkSendElectionEntry(vin, addr, vchSig, sigTime, pubkey, pubkey2, count, current, lastUpdated, protocolVersion);

        } else {
            LogPrintf("dsee - Rejected councilor entry %s\n", addr.ToString().c_str());

            int nDoS = 0;
            if (state.IsInvalid(nDoS))
            {
                LogPrintf("dsee - %s from %s %s was not accepted into the memory pool\n", tx.GetHash().ToString().c_str(),
                    pfrom->addr.ToString().c_str(), pfrom->cleanSubVer.c_str());
                if (nDoS > 0)
                    pfrom->Misbehaving(nDoS);
            }
        }
    }

    else if (strCommand == "dseep") { //DarkSend Election Entry Ping
        bool fIsInitialDownload = IsInitialBlockDownload();
        if(fIsInitialDownload) return;

        CTxIn vin;
        vector<unsigned char> vchSig;
        int64 sigTime;
        bool stop;
        vRecv >> vin >> vchSig >> sigTime >> stop;

        if (sigTime > GetAdjustedTime() + 60 * 60) {
            LogPrintf("dseep - Signature rejected, too far into the future %s\n", vin.ToString().c_str());
            return;
        }

        if (sigTime <= GetAdjustedTime() - 60 * 60) {
            LogPrintf("dseep - Signature rejected, too far into the past %s - %"PRI64u" %"PRI64u" \n", vin.ToString().c_str(), sigTime, GetAdjustedTime());
            return;
        }

        // see if we have this councilor

        BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors) {
            if(mn.vin.prevout == vin.prevout) {
                // take this only if it's newer
                if(mn.lastDseep < sigTime){ 
                    std::string strMessage = mn.addr.ToString() + boost::lexical_cast<std::string>(sigTime) + boost::lexical_cast<std::string>(stop);

                    std::string errorMessage = "";
                    if(!darkSendSigner.VerifyMessage(mn.pubkey2, vchSig, strMessage, errorMessage)){
                        LogPrintf("dseep - Got bad councilor address signature %s \n", vin.ToString().c_str());
                        //pfrom->Misbehaving(20);
                        return;
                    }

                    mn.lastDseep = sigTime;

                    if(!mn.UpdatedWithin(COUNCILOR_MIN_DSEEP_SECONDS)){
                        mn.UpdateLastSeen();
                        if(stop) {
                            mn.Disable();
                            mn.Check();
                        }
                        RelayDarkSendElectionEntryPing(vin, vchSig, sigTime, stop);
                    }
                }
                return;
            }
        }

        if(fDebug) LogPrintf("dseep - Couldn't find councilor entry %s\n", vin.ToString().c_str());

        std::map<COutPoint, int64>::iterator i = askedForCouncilorListEntry.find(vin.prevout);
        if (i != askedForCouncilorListEntry.end()){
            int64 t = (*i).second;
            if (GetTime() < t) {
                // we've asked recently
                return;
            }
        }

        // ask for the dsee info once from the node that sent dseep

        LogPrintf("dseep - Asking source node for missing entry %s\n", vin.ToString().c_str());
        pfrom->PushMessage("dseg", vin);
        int64 askAgain = GetTime()+(60*60*24);
        askedForCouncilorListEntry[vin.prevout] = askAgain;

    } else if (strCommand == "dseg") { //Get councilor list or specific entry
        CTxIn vin;
        vRecv >> vin;

        if(vin == CTxIn()) { //only should ask for this once
            //local network
            if(!pfrom->addr.IsRFC1918()) 
            {
                std::map<CNetAddr, int64>::iterator i = askedForCouncilorList.find(pfrom->addr);
                if (i != askedForCouncilorList.end())
                {
                    int64 t = (*i).second;
                    if (GetTime() < t) {
                        pfrom->Misbehaving(100);
                        LogPrintf("dseg - peer already asked me for the list\n");
                        return;
                    }
                }

                int64 askAgain = GetTime()+(60*60*24);
                askedForCouncilorList[pfrom->addr] = askAgain;
            }
        } //else, asking for a specific node which is ok

        int count = darkSendCounciLors.size()-1;
        int i = 0;

        BOOST_FOREACH(CCounciLor mn, darkSendCounciLors) {

            if(mn.addr.IsRFC1918()) continue; //local network

            if(vin == CTxIn()){
                mn.Check();
                if(mn.IsEnabled()) {
                    if(fDebug) LogPrintf("dseg - Sending councilor entry - %s \n", mn.addr.ToString().c_str());
                    pfrom->PushMessage("dsee", mn.vin, mn.addr, mn.sig, mn.now, mn.pubkey, mn.pubkey2, count, i, mn.lastTimeSeen, mn.protocolVersion);
                }
            } else if (vin == mn.vin) {
                if(fDebug) LogPrintf("dseg - Sending councilor entry - %s \n", mn.addr.ToString().c_str());
                pfrom->PushMessage("dsee", mn.vin, mn.addr, mn.sig, mn.now, mn.pubkey, mn.pubkey2, count, i, mn.lastTimeSeen, mn.protocolVersion);
                LogPrintf("dseg - Sent 1 councilor entries to %s\n", pfrom->addr.ToString().c_str());
                return;
            }
            i++;
        }

        LogPrintf("dseg - Sent %d councilor entries to %s\n", count, pfrom->addr.ToString().c_str());
    }

    else if (strCommand == "mnget") { //Councilor Payments Request Sync

        if(pfrom->HasFulfilledRequest("mnget")) {
            LogPrintf("mnget - peer already asked me for the list\n");
            pfrom->Misbehaving(20);
            return;
        }

        pfrom->FulfilledRequest("mnget");
        councilorPayments.Sync(pfrom);
        LogPrintf("mnget - Sent councilor winners to %s\n", pfrom->addr.ToString().c_str());
    }
    else if (strCommand == "mnw") { //Councilor Payments Declare Winner
        CCouncilorPaymentWinner winner;
        vRecv >> winner;

        if(pindexBest == NULL) return;

        uint256 hash = winner.GetHash();
        if(mapSeenCouncilorVotes.count(hash)) {
            if(fDebug) LogPrintf("mnw - seen vote %s Height %d bestHeight %d\n", hash.ToString().c_str(), winner.nBlockHeight, pindexBest->nHeight);
            return;
        }

        if(winner.nBlockHeight < pindexBest->nHeight - 10 || winner.nBlockHeight > pindexBest->nHeight+20){
            LogPrintf("mnw - winner out of range %s Height %d bestHeight %d\n", winner.vin.ToString().c_str(), winner.nBlockHeight, pindexBest->nHeight);
            return;
        }

        if(winner.vin.nSequence != std::numeric_limits<unsigned int>::max()){
            LogPrintf("mnw - invalid nSequence\n");
            pfrom->Misbehaving(100);
            return;
        }

        LogPrintf("mnw - winning vote  %s Height %d bestHeight %d\n", winner.vin.ToString().c_str(), winner.nBlockHeight, pindexBest->nHeight);

        if(!councilorPayments.CheckSignature(winner)){
            LogPrintf("mnw - invalid signature\n");
            pfrom->Misbehaving(100);
            return;
        }

        mapSeenCouncilorVotes.insert(make_pair(hash, 1));

        if(councilorPayments.AddWinningCouncilor(winner)){
            councilorPayments.Relay(winner);
        }
    } /*else if (strCommand == "mnse") { //Councilor Scanning Error
        CCouncilorScanningError entry;
        vRecv >> entry;

        if(pindexBest == NULL) return;

        uint256 hash = entry.GetHash();
        if(mapSeenCouncilorScanningErrors.count(hash)) {
            if(fDebug) LogPrintf("mnse - seen entry addr %d error %d\n", entry.addr.ToString().c_str(), entry.error.c_str());
            return;
        }

        LogPrintf("mnse - seen entry addr %d error %d\n", entry.addr.ToString().c_str(), entry.error.c_str());

        if(!councilorScanningError.CheckSignature(entry)){
            LogPrintf("mnse - invalid signature\n");
            pfrom->Misbehaving(100);
            return;
        }

        mapSeenCouncilorVotes.insert(make_pair(hash, 1));

        if(councilorScanningError.AddWinningCouncilor(entry)){
            councilorScanningError.Relay(entry);
        }
    }*/
}

struct CompareValueOnly
{
    bool operator()(const pair<int64, CTxIn>& t1,
                    const pair<int64, CTxIn>& t2) const
    {
        return t1.first < t2.first;
    }
};

struct CompareValueOnly2
{
    bool operator()(const pair<int64, int>& t1,
                    const pair<int64, int>& t2) const
    {
        return t1.first < t2.first;
    }
};

int CountCouncilorsAboveProtocol(int protocolVersion)
{
    int i = 0;

    BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors) {
        if(mn.protocolVersion < protocolVersion) continue;
        i++;
    }

    return i;

}


int GetCouncilorByVin(CTxIn& vin)
{
    int i = 0;

    BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors) {
        if (mn.vin == vin) return i;
        i++;
    }

    return -1;
}

int GetCurrentCounciLor(int mod, int64 nBlockHeight, int minProtocol)
{
    int i = 0;
    unsigned int score = 0;
    int winner = -1;

    // scan for winner
    BOOST_FOREACH(CCounciLor mn, darkSendCounciLors) {
        mn.Check();
        if(mn.protocolVersion < minProtocol) continue;
        if(!mn.IsEnabled()) {
            i++;
            continue;
        }

        // calculate the score for each councilor
        uint256 n = mn.CalculateScore(mod, nBlockHeight);
        unsigned int n2 = 0;
        memcpy(&n2, &n, sizeof(n2));

        // determine the winner
        if(n2 > score){
            score = n2;
            winner = i;
        }
        i++;
    }

    return winner;
}

int GetCouncilorByRank(int findRank, int64 nBlockHeight, int minProtocol)
{
    int i = 0;

    std::vector<pair<unsigned int, int> > vecCouncilorScores;

    i = 0;
    BOOST_FOREACH(CCounciLor mn, darkSendCounciLors) {
        mn.Check();
        if(mn.protocolVersion < minProtocol) continue;
        if(!mn.IsEnabled()) {
            i++;
            continue;
        }

        uint256 n = mn.CalculateScore(1, nBlockHeight);
        unsigned int n2 = 0;
        memcpy(&n2, &n, sizeof(n2));

        vecCouncilorScores.push_back(make_pair(n2, i));
        i++;
    }

    sort(vecCouncilorScores.rbegin(), vecCouncilorScores.rend(), CompareValueOnly2());

    int rank = 0;
    BOOST_FOREACH (PAIRTYPE(unsigned int, int)& s, vecCouncilorScores){
        rank++;
        if(rank == findRank) return s.second;
    }

    return -1;
}

int GetCouncilorRank(CTxIn& vin, int64 nBlockHeight, int minProtocol)
{
    std::vector<pair<unsigned int, CTxIn> > vecCouncilorScores;

    BOOST_FOREACH(CCounciLor mn, darkSendCounciLors) {
        mn.Check();
        if(mn.protocolVersion < minProtocol) continue;
        if(!mn.IsEnabled()) {
            continue;
        }

        uint256 n = mn.CalculateScore(1, nBlockHeight);
        unsigned int n2 = 0;
        memcpy(&n2, &n, sizeof(n2));

        vecCouncilorScores.push_back(make_pair(n2, mn.vin));
    }

    sort(vecCouncilorScores.rbegin(), vecCouncilorScores.rend(), CompareValueOnly());

    unsigned int rank = 0;
    BOOST_FOREACH (PAIRTYPE(unsigned int, CTxIn)& s, vecCouncilorScores){
        rank++;
        if(s.second == vin) return rank;
    }

    return -1;
}

//
// Deterministically calculate a given "score" for a councilor depending on how close it's hash is to
// the proof of work for that block. The further away they are the better, the furthest will win the election
// and get paid this block
//
uint256 CCounciLor::CalculateScore(int mod, int64 nBlockHeight)
{
    if(pindexBest == NULL) return 0;

    uint256 hash = 0;
    if(!darkSendPool.GetLastValidBlockHash(hash, mod, nBlockHeight)) return 0;
    uint256 hash2 = HashX11(BEGIN(hash), END(hash));

    // we'll make a 4 dimensional point in space
    // the closest councilor to that point wins
    uint64 a1 = hash2.Get64(0);
    uint64 a2 = hash2.Get64(1);
    uint64 a3 = hash2.Get64(2); 
    uint64 a4 = hash2.Get64(3);

    //copy part of our source hash
    int i1, i2, i3, i4;
    i1=0;i2=0;i3=0;i4=0;
    memcpy(&i1, &a1, 1);
    memcpy(&i2, &a2, 1);
    memcpy(&i3, &a3, 1);
    memcpy(&i4, &a4, 1);

    //split up our mn hash
    uint64 b1 = vin.prevout.hash.Get64(0);
    uint64 b2 = vin.prevout.hash.Get64(1);
    uint64 b3 = vin.prevout.hash.Get64(2);
    uint64 b4 = vin.prevout.hash.Get64(3);

    //move mn hash around
    b1 <<= (i1 % 64);
    b2 <<= (i2 % 64);
    b3 <<= (i3 % 64);
    b4 <<= (i4 % 64);

    // calculate distance between target point and mn point
    uint256 r = 0;
    r +=  (a1 > b1 ? a1 - b1 : b1 - a1); 
    r +=  (a2 > b2 ? a2 - b2 : b2 - a2);
    r +=  (a3 > b3 ? a3 - b3 : b3 - a3);
    r +=  (a4 > b4 ? a4 - b4 : b4 - a4);

    /*
    LogPrintf(" -- CounciLor CalculateScore() n2 = %s \n", n2.ToString().c_str());
    LogPrintf(" -- CounciLor CalculateScore() vin = %s \n", vin.prevout.hash.GetHex().c_str());
    LogPrintf(" -- CounciLor CalculateScore() n3 = %s \n", n3.ToString().c_str());*/

    return r;
}

void CCounciLor::Check()
{
    //once spent, stop doing the checks
    if(enabled==3) return;


    if(!UpdatedWithin(COUNCILOR_REMOVAL_SECONDS)){
        enabled = 4;
        return;
    }

    if(!UpdatedWithin(COUNCILOR_EXPIRATION_SECONDS)){
        enabled = 2;
        return;
    }

    if(!unitTest){
        CValidationState state;
        CTransaction tx = CTransaction();
        CTxOut vout = CTxOut(999.99*COIN, darkSendPool.collateralPubKey);
        tx.vin.push_back(vin);
        tx.vout.push_back(vout);

        if(!tx.AcceptableInputs(state, true)) {
            enabled = 3;
            return;
        }
    }

    enabled = 1; // OK
}

bool CCouncilorPayments::CheckSignature(CCouncilorPaymentWinner& winner)
{
    std::string strMessage = winner.vin.ToString().c_str() + boost::lexical_cast<std::string>(winner.nBlockHeight);
    std::string strPubKey = fTestNet? strTestPubKey : strMainPubKey;
    CPubKey pubkey(ParseHex(strPubKey));

    std::string errorMessage = "";
    if(!darkSendSigner.VerifyMessage(pubkey, winner.vchSig, strMessage, errorMessage)){
        return false;
    }

    return true;
}

bool CCouncilorPayments::Sign(CCouncilorPaymentWinner& winner)
{
    std::string strMessage = winner.vin.ToString().c_str() + boost::lexical_cast<std::string>(winner.nBlockHeight);

    CKey key2;
    CPubKey pubkey2;
    std::string errorMessage = "";

    if(!darkSendSigner.SetKey(strMasterPrivKey, errorMessage, key2, pubkey2))
    {
        LogPrintf("CCouncilorPayments::Sign - Invalid councilorprivkey: '%s'\n", errorMessage.c_str());
        exit(0);
    }

    if(!darkSendSigner.SignMessage(strMessage, errorMessage, winner.vchSig, key2)) {
        LogPrintf("CCouncilorPayments::Sign - Sign message failed");
        return false;
    }

    if(!darkSendSigner.VerifyMessage(pubkey2, winner.vchSig, strMessage, errorMessage)) {
        LogPrintf("CCouncilorPayments::Sign - Verify message failed");
        return false;
    }

    return true;
}

uint64 CCouncilorPayments::CalculateScore(uint256 blockHash, CTxIn& vin)
{
    uint256 n1 = blockHash;
    uint256 n2 = HashX11(BEGIN(n1), END(n1));
    uint256 n3 = HashX11(BEGIN(vin.prevout.hash), END(vin.prevout.hash));
    uint256 n4 = n3 > n2 ? (n3 - n2) : (n2 - n3);

    //printf(" -- CCouncilorPayments CalculateScore() n2 = %"PRI64u" \n", n2.Get64());
    //printf(" -- CCouncilorPayments CalculateScore() n3 = %"PRI64u" \n", n3.Get64());
    //printf(" -- CCouncilorPayments CalculateScore() n4 = %"PRI64u" \n", n4.Get64());

    return n4.Get64();
}

bool CCouncilorPayments::GetBlockPayee(int nBlockHeight, CScript& payee)
{
    BOOST_FOREACH(CCouncilorPaymentWinner& winner, vWinning){
        if(winner.nBlockHeight == nBlockHeight) {

            CTransaction tx;
            uint256 hash;
            if(GetTransaction(winner.vin.prevout.hash, tx, hash, true)){
                BOOST_FOREACH(CTxOut out, tx.vout){
                    if(out.nValue == 10000*COIN){
                        payee = out.scriptPubKey;
                        return true;
                    }
                }
            }

            return false;
        }
    }

    return false;
}

bool CCouncilorPayments::GetWinningCouncilor(int nBlockHeight, CTxIn& vinOut)
{
    BOOST_FOREACH(CCouncilorPaymentWinner& winner, vWinning){
        if(winner.nBlockHeight == nBlockHeight) {
            vinOut = winner.vin;
            return true;
        }
    }

    return false;
}

bool CCouncilorPayments::AddWinningCouncilor(CCouncilorPaymentWinner& winnerIn)
{
    uint256 blockHash = 0;
    if(!darkSendPool.GetBlockHash(blockHash, winnerIn.nBlockHeight-576)) {
        return false;
    }

    winnerIn.score = CalculateScore(blockHash, winnerIn.vin);

    bool foundBlock = false;
    BOOST_FOREACH(CCouncilorPaymentWinner& winner, vWinning){
        if(winner.nBlockHeight == winnerIn.nBlockHeight) {
            foundBlock = true;
            if(winner.score < winnerIn.score){
                winner.score = winnerIn.score;
                winner.vin = winnerIn.vin;
                winner.vchSig = winnerIn.vchSig;
                return true;
            }
        }
    }

    // if it's not in the vector
    if(!foundBlock){
         vWinning.push_back(winnerIn);
         return true;
    }

    return false;
}

void CCouncilorPayments::CleanPaymentList()
{
    if(pindexBest == NULL) return;

    vector<CCouncilorPaymentWinner>::iterator it;
    for(it=vWinning.begin();it<vWinning.end();it++){
        if(pindexBest->nHeight - (*it).nBlockHeight > 1000){
            if(fDebug) LogPrintf("CCouncilorPayments::CleanPaymentList - Removing old councilor payment - block %d\n", (*it).nBlockHeight);
            vWinning.erase(it);
            break;
        }
    }
}

int CCouncilorPayments::LastPayment(CCounciLor& mn)
{
    if(pindexBest == NULL) return 0;

    int ret = mn.GetCouncilorInputAge();

    BOOST_FOREACH(CCouncilorPaymentWinner& winner, vWinning){
        if(winner.vin == mn.vin && pindexBest->nHeight - winner.nBlockHeight < ret)
            ret = pindexBest->nHeight - winner.nBlockHeight;
    }

    return ret;
}

bool CCouncilorPayments::ProcessBlock(int nBlockHeight)
{
    if(strMasterPrivKey.empty()) return false;
    CCouncilorPaymentWinner winner;

    uint256 blockHash = 0;
    if(!darkSendPool.GetBlockHash(blockHash, nBlockHeight-576)) return false;

    BOOST_FOREACH(CCounciLor& mn, darkSendCounciLors) {
        mn.Check();

        if(!mn.IsEnabled()) {
            continue;
        }

        if(LastPayment(mn) < darkSendCounciLors.size()*.9) continue;

        uint64 score = CalculateScore(blockHash, mn.vin);
        if(score > winner.score){
            winner.score = score;
            winner.nBlockHeight = nBlockHeight;
            winner.vin = mn.vin;
        }
    }

    if(winner.nBlockHeight == 0) return false; //no councilors available

    if(Sign(winner)){
        if(AddWinningCouncilor(winner)){
            Relay(winner);
            return true;
        }
    }

    return false;
}

void CCouncilorPayments::Relay(CCouncilorPaymentWinner& winner)
{
    LOCK(cs_vNodes);
    BOOST_FOREACH(CNode* pnode, vNodes){
        if(!pnode->fRelayTxes)
            continue;

        pnode->PushMessage("mnw", winner);
    }
}

void CCouncilorPayments::Sync(CNode* node)
{
    BOOST_FOREACH(CCouncilorPaymentWinner& winner, vWinning)
        if(winner.nBlockHeight >= pindexBest->nHeight-10 && winner.nBlockHeight <= pindexBest->nHeight + 20)
            node->PushMessage("mnw", winner);
}


bool CCouncilorPayments::SetPrivKey(std::string strPrivKey)
{
    CCouncilorPaymentWinner winner;

    // Test signing successful, proceed
    strMasterPrivKey = strPrivKey;

    Sign(winner);

    if(CheckSignature(winner)){
        LogPrintf("CCouncilorPayments::SetPrivKey - Successfully initialized as councilor payments master\n");
        return true;
    } else {
        return false;
    }
}
